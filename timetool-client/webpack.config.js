
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const babelLoaderConfiguration = {
  test: /\.js$/,
  include: [
    path.resolve(__dirname, 'src'),
    path.resolve(__dirname, 'node_modules/react-native-uncompiled'),
    path.resolve(__dirname, 'node_modules/react-router-native'),
    path.resolve(__dirname, 'node_modules/react-native-vector-icons')
  ],
  use: {
    loader: 'babel-loader',
    options: {
      cacheDirectory: true,
      presets: ['react-native']
    }
  }
};

const imageLoaderConfiguration = {
  test: /\.(gif|jpe?g|png|svg)$/,
  use: {
    loader: 'url-loader',
    options: {
      name: '[name].[ext]'
    }
  }
};

const fontLoaderConfiguration = {
  test: /\.ttf$/,
  loader: 'url-loader',
  include: path.resolve(__dirname, 'node_modules/react-native-vector-icons'),
};

const configurations = module.exports = [];

console.info(`NODE_ENV=${process.env.NODE_ENV}`);
console.info(`WEBPACK_TARGET=${process.env.WEBPACK_TARGET}`);

configurations.push({
  entry: {
    index: './index'
  },

  output: {
    path: path.resolve(__dirname, 'lib'),
    filename: '[name].js'
  },

  target: process.env.WEBPACK_TARGET || 'electron-renderer',

  module: {
    rules: [
      babelLoaderConfiguration,
      imageLoaderConfiguration,
      fontLoaderConfiguration
    ]
  },

  devtool: 'inline-source-map',

  devServer: {
    historyApiFallback: true,
    contentBase: './src'
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV === 'production' ? 'production' : 'development'),
      },
      '__DEV__': JSON.stringify(process.env.NODE_ENV === 'production' ? false : true),
      WEBPACK_TARGET: JSON.stringify(process.env.WEBPACK_TARGET || 'electron-renderer'),
      'Platform': {
        'OS': JSON.stringify(process.env.WEBPACK_TARGET || 'electron')
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],

  resolve: {
    alias: {
      'react-native': 'react-native-web',
      'react-native-keyboard-aware-scroll-view': 'empty-module'
    },
    extensions: [ process.env.NODE_ENV === 'production' ? '.prod.js' : '.dev.js', `.${process.env.WEBPACK_TARGET || 'electron'}.js`, '.js' ]
  },

  node: {
    __dirname: false,
    __filename: false
  }
});

/*
if(process.env.NODE_ENV === 'production') {
  configurations.push({
    entry: {
      electron: './src/electron'
    },

    target: 'electron-main',

    output: {
      path: path.resolve(__dirname, 'lib'),
      filename: '[name].js'
    },

    externals: [
      'electron-named-image'
    ],

    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader'
          }
        }
      ]
    },

    devtool: 'inline-source-map',

    resolve: {
      extensions: [ '.prod.js', '.electron.js', '.js' ]
    },

    node: {
      __dirname: false,
      __filename: false
    }
  });
}
*/
