#!/usr/bin/env node

const fs = require('fs');
const path = require('path');
const packager = require('electron-packager');
const { rebuild } = require('electron-rebuild');
const childProcess = require('child_process');

const build = (type) => new Promise((resolve, reject) => {
  console.info("\nPackaging app for macOS App Store", type);

  packager({
    dir: path.resolve(__dirname, '..'),
    name: 'Timetool',
    appCopyright: '2017 Grexie Limited',
    asar: true,
    platform: 'mas',
    appBundleId: 'com.grexie.TimetoolMac',
    out: 'dist',
    icon: 'icon.icns',
    ignore: [
      /^\/src(\/|$)/,
      /^\/android(\/|$)/,
      /^\/ios(\/|$)/,
      /^\/macos(\/|$)/,
      /^\/icon\.\w+$/,
      /^\/[\w\.-]+\.js$/,
      /^\/package-lock\.json$/,
      /^\/app\.json$/,
      /^\/yarn[\w\.-]+$/,
      /^\/node_modules(\/|$)/
    ],
    osxSign: {
      type,
      'entitlements': path.resolve(__dirname, `mas.${type}.entitlements`),
      'entitlements-inherit': path.resolve(__dirname, `mas.inherit.${type}.entitlements`),
      'provisioning-profile': path.resolve(__dirname, `timetool-${type}.provisionprofile`)
    },
    prune: false,
    quiet: false,
    overwrite: true,
    afterCopy: [(buildPath, electronVersion, platform, arch, callback) => {
      fs.writeFileSync(path.resolve(buildPath, 'lib', 'env.json'), JSON.stringify({ NODE_ENV: 'production' }));

      const pkgJson = JSON.parse(fs.readFileSync(path.resolve(buildPath, 'package.json')));
      const newPkgJson = {
        name: pkgJson.name,
        version: pkgJson.version,
        description: pkgJson.description,
        main: pkgJson.main,
        license: pkgJson.license,
        private: pkgJson.private,
        dependencies: pkgJson.dependencies
      };
      delete newPkgJson.dependencies['empty-module'];
      delete newPkgJson.dependencies['events'];
      delete newPkgJson.dependencies['react-native'];
      delete newPkgJson.dependencies['react-native-vector-icons'];
      delete newPkgJson.dependencies['react-native-version'];
      console.info('Re-writing package.json for production...');
      fs.writeFileSync(path.resolve(buildPath, 'package.json'), JSON.stringify(newPkgJson, null, 2));

      console.info('Installing production dependencies...')
      childProcess.execSync('yarn install --no-progress --prod', { cwd: buildPath, stdio: 'inherit' });

      callback();
    }]
  }, (err, [appPath]) => {
    if(err) {
      reject(err);
      return;
    }

    console.info('Creating installer package...');
    childProcess.execSync(`electron-osx-flat ${appPath}/Timetool.app --identity "3rd Party Mac Developer Installer: Grexie Limited (RY2J5VXCDH)" --pkg dist/timetool-macos-${type}.pkg`);
    resolve();
  });
});

build('development').then(() => build('distribution')).catch(err => console.error(err.stack));
