

import SimpleLineIcons from "react-native-vector-icons/Fonts/SimpleLineIcons.ttf"
const reactNativeVectorIconsRequiredStyles = "@font-face { src:url("+SimpleLineIcons+");font-family: 'simple-line-icons'; }"

// create stylesheet
const style = document.createElement('style');
style.type = 'text/css';
if (style.styleSheet){
  style.styleSheet.cssText = reactNativeVectorIconsRequiredStyles;
} else {
  style.appendChild(document.createTextNode(reactNativeVectorIconsRequiredStyles));
}

// inject stylesheet
document.head.appendChild(style);

import './src';
