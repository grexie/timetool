export default {
  URL_PREFIX: 'https://timetool-dev.grexie.com/api',
  WEBSOCKET_ADDRESS: 'wss://timetool-dev.grexie.com/',
  backgroundColor: '#46d',
  borderColor: '#248'
};
