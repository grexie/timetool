export default {
  URL_PREFIX: 'https://timetool.grexie.com/api',
  WEBSOCKET_ADDRESS: 'wss://timetool.grexie.com/',
  backgroundColor: '#46d',
  borderColor: '#248'
};
