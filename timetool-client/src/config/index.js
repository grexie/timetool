
if(!__DEV__) {
  module.exports = require('./config.prod.js');
} else {
  module.exports = require('./config.dev.js');
}
