
import express from 'express';
import path from 'path';

const app = express();

app.use('/index.js', express.static(path.resolve(__dirname, '..', 'index.js')));
app.get('/+', (req, res) => res.sendFile(path.resolve(__dirname, '..', 'index.html')));

app.listen('8080', () => {
  console.info('Listening on port 8080');
});
