
import { Platform } from 'react-native';


if(typeof WEBPACK_TARGET !== 'undefined') {
  if(WEBPACK_TARGET === 'electron-renderer') {
    Platform.OS = 'electron';
  }
}
