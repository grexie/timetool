
try {
  const env = require('./env.json');
  for(var v in env) process.env[v] = env[v];
} catch(err) {

}

global.WEBPACK_TARGET = 'electron';

if(process.env.NODE_ENV === 'production') {
  global.__DEV__ = false;
  require.extensions['.prod.js'] = (code) => code;
  require.extensions['.electron.js'] = (code) => code;
} else {
  global.__DEV__ = true;
  require.extensions['.dev.js'] = (code) => code;
  require.extensions['.electron.js'] = (code) => code;
  delete require.extensions['.js'];
  require.extensions['.js'] = (code) => code;
  require('babel-core/register')({
    extensions: ['.dev.js', '.electron.js', '.js']
  });
}

const { app } = require('electron');
const path = require('path');
const ServiceManager = require('./services').default;
const fetch = require('node-fetch');
const WebSocket = require('ws');
global.fetch = fetch;
global.WebSocket = WebSocket;

const createServiceManager = (options) => {
  global.serviceManager = new ServiceManager(options);
  const { windowService, userService } = serviceManager;

  windowService.get('signin').on('close', () => {
    if(!userService.user) {
      app.emit('before-quit');
      app.quit();
    }
  });
};

app.on('ready', () => {
  if(process.env.NODE_ENV === 'production') {
    const express = require('express');
    const app = express();

    app.get('/index.js', (req, res) => res.sendFile(path.resolve(__dirname, 'index.js')));
    app.get('/network-status.html', (req, res) => res.sendFile(path.resolve(__dirname, 'network-status.html')));
    app.get('/*', (req, res) => res.sendFile(path.resolve(__dirname, 'index.html')));

    const server = app.listen(0, '127.0.0.1', () => {
      createServiceManager({
        port: server.address().port
      });
    });
  } else {
    createServiceManager({
      port: 8080
    })
  }
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});
