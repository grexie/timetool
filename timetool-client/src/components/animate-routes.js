
import React from 'react';
import {
  Animated,
  Dimensions,
  View,
  StyleSheet,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import { Switch } from 'react-router-native';

class AnimatedRouter extends React.Component {

  constructor() {
    super();
    this.state = {
      previousChildren: null,
      previousLocation: null
    };
    this.animation = new Animated.Value(0);
  }

  componentWillReceiveProps(nextProps) {
    const { animation } = this;
    const { animated, done } = nextProps.location.state || {};

    if(animated) {

      this.setState((state) => {
        state.previousChildren = this.props.children;
        state.previousLocation = this.props.location;

        animation.setValue(0);
        Animated.timing(animation, {
          toValue: 1
        }).start(() => {
          this.setState({ previousChildren: null, previousLocation: null });
          if(done) {
            done();
          }
        });
      });
    } else {
      animation.setValue(1);
      if(done) {
        done();
      }
    }
  }

  render() {
    const { animation } = this;
    const { previousChildren, previousLocation } = this.state;
    const { children: nextChildren, location: nextLocation } = this.props;

    let { animated } = nextLocation.state || {};
    if(animated === true) {
      animated = 'slide-left';
    }

    const device = Dimensions.get('window');

    const previousTransforms = [];
    const nextTransforms = [];
    let reverse = false;

    if(animated === 'slide-left') {
      reverse = true;

      previousTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -device.width ]
        })
      });

      nextTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ device.width, 0 ]
        })
      });
    }

    if(animated === 'slide-right') {
      previousTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, device.width ]
        })
      });

      nextTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ -device.width, 0 ]
        })
      });
    }

    if(animated === 'slide-up') {
      reverse = true;

      previousTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -device.height ]
        })
      });

      nextTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ device.height, 0 ]
        })
      });
    }

    if(animated === 'slide-down') {
      previousTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, device.height ]
        })
      });

      nextTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ -device.height, 0 ]
        })
      });
    }

    if(previousLocation === null) {
      return (
        <View style={{flex: 1}}>
          <Animated.View key=".1" style={[StyleSheet.absoluteFill]}>
            <Switch location={nextLocation}>
              {nextChildren}
            </Switch>
          </Animated.View>
        </View>
      );
    }

    if(reverse) {
      return (
        <View style={{flex: 1}}>
          <Animated.View key=".1" style={[StyleSheet.absoluteFill, { transform: nextTransforms }]}>
            <Switch location={nextLocation}>
              {nextChildren}
            </Switch>
          </Animated.View>
          {previousChildren && <Animated.View key=".0" style={[StyleSheet.absoluteFill, { transform: previousTransforms }]}>
            <Switch location={previousLocation}>
              {previousChildren}
            </Switch>
          </Animated.View>}
        </View>
      );
    } else {
      return (
        <View style={{flex: 1}}>
          {previousChildren && <Animated.View key=".0" style={[StyleSheet.absoluteFill, { transform: previousTransforms }]}>
            <Switch location={previousLocation}>
              {previousChildren}
            </Switch>
          </Animated.View>}
          <Animated.View key=".1" style={[StyleSheet.absoluteFill, { transform: nextTransforms }]}>
            <Switch location={nextLocation}>
              {nextChildren}
            </Switch>
          </Animated.View>
        </View>
      );
    }

  }
};

export default class AnimateRoutes extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  render() {
    return (
      <AnimatedRouter location={this.context.router.history.location}>
        {this.props.children}
      </AnimatedRouter>
    );
  }
}
