
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import UserService from '../services/user-service';
import Form from './form';
import { RequireNetwork } from './router';
import Splash from './splash';
import Offline from './offline';

const styles = StyleSheet.create({

});

export default class SignUp extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: '',
      error: null
    };
  }

  onPressSignUp = async () => {
    const { userService } = this.context.serviceManager;
    const { name, email, password } = this.state;

    try {
      const user = await userService.create(name, email, password);
      this.props.history.replace('/', { animated: 'slide-down', window: 'home', showWindow: false, close: true });
    } catch(err) {
      this.setState({ error: err.message });
    }
  };

  onPressSignIn = async () => {
    const { history } = this.context;
    const { next = '/' } = history.location.state || {};
    history.replace('/signin', { animated: 'slide-left', next});
  };

  onChangeName = (name) => {
    name = name.replace(/\s+/g, ' ').replace(/^\s*/, '');
    this.setState({ name });
  };

  onChangeEmail = (email) => {
    email = email.trim().replace(/\s+/, '');
    this.setState({ email });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  render() {
    const  { name, email, password, error } = this.state;
    return (
      <RequireNetwork splash={() => <Splash/>} offline={(online, captive) => <Offline online={online} captive={captive}/>}>
        <Form
          error={error}
          fields={{
            name: {
              value: name,
              placeholder: 'Name',
              onChangeText: this.onChangeName,
              autoCapitalize: 'words',
              autoCorrect: false,
              keyboardType: 'default'
            },
            email: {
              value: email,
              placeholder: 'Email',
              onChangeText: this.onChangeEmail,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'email-address'
            },
            password: {
              value: password,
              placeholder: 'Password',
              onChangeText: this.onChangePassword,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'default',
              secureTextEntry: true
            }
          }}
          buttonTitle="Sign Up"
          onSubmit={this.onPressSignUp}
          footer={{
            signIn: {
              text: 'Already have an account?',
              title: 'Sign in',
              onPress: this.onPressSignIn
            }
          }}/>
      </RequireNetwork>
    );
  }
}
