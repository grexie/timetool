
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  Platform
} from 'react-native';
import Logo from './logo';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch'
  },
  header: {
    paddingLeft: 25,
    height: 120
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 30,
    backgroundColor: Platform.OS === 'electron' ? 'transparent' : '#f5fbff'
  },
  activityIndicator: {
    marginBottom: 20
  },
  text: {
    textAlign: 'center',
    fontSize: Platform.OS === 'electron' ? 14 : 20,
    fontWeight: '300'
  }
});

export default class Offline extends React.Component {
  render() {
    const { logo = true, online, captive } = this.props;

    return (
      <View style={styles.container}>
        {logo && <Logo style={styles.header}/>}
        <View style={styles.body}>
          <ActivityIndicator size="large" style={styles.activityIndicator}/>
          {!online && <Text style={styles.text}>Unable to connect to Timetool server. Please check your network connection.</Text>}
          {captive && <Text style={styles.text}>Unable to access Timetool server. Please test your network connection and try again.</Text>}
        </View>
      </View>
    );
  }
};
