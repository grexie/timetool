
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  ScrollView,
  Platform,
  KeyboardAvoidingView
} from 'react-native';
import PropTypes from 'prop-types';
import Button from './button';
import Logo from './logo';
import Config from '../config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  header: {
    paddingLeft: 25,
    height: 120
  },
  body: {
    paddingTop: 30,
    padding: 30,
    backgroundColor: '#f5fbff'
  },
  bodyContent: {
    paddingBottom: Platform.OS === 'android' ? 50 : 0
  },
  textInputContainer: {
    backgroundColor: 'white',
    borderColor: '#95989A',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 10,
    marginBottom: 20
  },
  textInput: {
    height: 50,
    color: 'black',
    fontSize: 16,
    fontWeight: '300',
    padding: 0,
    paddingHorizontal: 20,
  },
  button: {
    backgroundColor: '#3ec4ff',
    paddingVertical: 18,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowColor: 'black',
    shadowRadius: 0,
    shadowOpacity: 0.7
  },
  buttonActive: {
    shadowOpacity: 0,
    top: 1,
    opacity: 0.7
  },
  buttonText: {
    color: 'white',
    fontWeight: '600',
    /*textShadowOffset: {
      width: 0,
      height: 1
    },
    textShadowColor: 'black',
    textShadowOpacity: 0.4,*/
    fontSize: 16
  },
  buttonTextActive: {

  },
  footerContainer: {
    flexDirection: 'row',
    marginTop: 20,
    justifyContent: 'flex-end'
  },
  footerContainerText: {
    color: '#95989a',
    fontWeight: '300',
    fontSize: 14
  },
  footer: {
    marginLeft: 5
  },
  footerActive: {

  },
  footerText: {
    color: '#285d81',
    fontWeight: '300',
    fontSize: 14
  },
  footerTextActive: {
    opacity: 0.7
  },
  errorContainer: {
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#a78888',
    backgroundColor: '#ffe5e5',
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 20
  },
  errorText: {
    fontSize: 15,
    fontWeight: '400',
    color: 'black'
  },
  messageContainer: {
    backgroundColor: 'rgb(252,248,230)',
    borderColor: 'rgb(237,231,192)',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    marginBottom: 20
  },
  messageText: {
    fontSize: 15,
    fontWeight: '400',
    color: 'black'
  },
});

export default class Form extends React.Component {
  constructor() {
    super();
  }

  onSubmitEditing = (name) => {
    const { fields, onSubmit } = this.props;

    let isNext = false;
    for(const nextName in fields) {
      if(isNext) {
        this.refs[nextName].focus();
        return;
      }

      if(nextName === name) {
        isNext = true;

      }
    }

    onSubmit();
  };

  renderField(name, field) {
    const { fields } = this.props;
    const isFirst = Object.keys(fields).shift() === name;
    const isLast = Object.keys(fields).pop() === name;

    return (
      <View key={name} style={styles.textInputContainer}>
        <TextInput
          ref={name}
          autoFocus={isFirst}
          returnKeyType={isLast ? 'next' : 'done'}
          underlineColorAndroid="transparent"
          onSubmitEditing={() => this.onSubmitEditing(name)}
          style={styles.textInput}
          {...field}/>
      </View>
    );
  }

  renderFooter(name, footer) {
    return (
      <View key={name} style={styles.footerContainer}>
        <Text style={styles.footerContainerText}>{footer.text}</Text>
        <Button
          style={styles.footer}
          activeStyle={styles.footerActive}
          textActiveStyle={styles.footerTextActive}
          onPress={footer.onPress}><Text style={styles.footerText}>{footer.title}</Text></Button>
      </View>
    );
  }

  render() {
    const  { header, error, message, fields, buttonTitle, onSubmit, footer } = this.props;

    const KAV = (contents) => Platform.OS !== 'ios' ? <View style={{flex: 1}}>{contents}</View> : (
      <KeyboardAvoidingView behavior="padding" style={{flex: 1}}>
        {contents}
      </KeyboardAvoidingView>
    );

    return (
      <View style={styles.container}>
        {KAV(
          <View style={{flex: 1}}>
            {!header && <Logo style={styles.header}/>}
            {header && header()}
            <ScrollView style={[{flex: 1}, styles.body]} contentContainerStyle={styles.bodyContent}>
              {message && <View style={styles.messageContainer}>
                <Text style={styles.messageText}>{message}</Text>
              </View>}
              {error && <View style={styles.errorContainer}>
                <Text style={styles.errorText}>{error}</Text>
              </View>}
              {Object.keys(fields).map(name => this.renderField(name, fields[name]))}
              <Button
                style={styles.button}
                activeStyle={styles.buttonActive}
                textStyle={styles.buttonText}
                textActiveStyle={styles.buttonTextActive}
                onPress={onSubmit}><Text style={styles.buttonText}>{buttonTitle.toUpperCase()}</Text></Button>
              {Object.keys(footer).map(name => this.renderFooter(name, footer[name]))}
            </ScrollView>
          </View>
        )}
      </View>
    );
  }
}
