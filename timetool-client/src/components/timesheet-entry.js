
import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Button from './button';
import moment from 'moment';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { formatHours } from '../utils';

const styles = StyleSheet.create({

  entryButtonView: {
    width: 32,
    height: 32,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3ec4ff',
    borderRadius: 23,
    marginLeft: 5
  },
  entryButtonViewRunning: {
    backgroundColor: '#ffc40c'
  },
  entryButton: {
    marginLeft: 15
  },
  entryButtonImage: {
    width: 20,
    height: 20
  },
  entryContainer: {
    backgroundColor: '#f7f7f7',
    marginTop: 7,
    padding: 8,
    borderColor: '#eee',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderStyle: 'solid'
  },
  entryTimeText: {
    fontSize: 12,
    fontWeight: '300'
  },
  entryText: {
    fontSize: 14,
    fontWeight: '300'
  },
  entryDetailView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 15
  },
  entryContainerRunning: {
    backgroundColor: '#e2f3ff'
  }
});

export default class TimesheetEntry extends React.Component {

  componentWillMount() {
    if(this.props.time) this.props.time.on('update', this.onUpdateTime);
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.time) this.props.time.removeListener('update', this.onUpdateTime);
    if(nextProps.time) nextProps.time.on('update', this.onUpdateTime);
  }

  componentWillUnmount() {
    if(this.props.time) this.props.time.removeListener('update', this.onUpdateTime);
  }

  onUpdateTime = () => {
    this.forceUpdate();
  }

  render() {
    const { time, onPress } = this.props;

    return (
      <View style={[styles.entryContainer, time.running ? styles.entryContainerRunning : null]}>
        <View style={styles.entryTextView}>
          <Text style={styles.entryText}>{time.description}</Text>
        </View>
        <View style={styles.entryDetailView}>
          <Text style={styles.entryTimeText}>{formatHours(time.duration)}</Text>
          <Button onPress={onPress} style={[styles.entryButtonView, time.running ? styles.entryButtonViewRunning : null]}>
            {time.running && <Icon name="control-pause" size={14} color="white"/>}
            {!time.running && <Icon name="control-play" size={14} color="white"/>}
          </Button>
        </View>
      </View>
    );
  }
}
