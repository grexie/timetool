
import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  Platform,
  UIManager,
  findNodeHandle,
  LayoutAnimation,
  KeyboardAvoidingView,
  Keyboard
} from 'react-native';
import Button from './button';
import PropTypes from 'prop-types';
import Logo from './logo';
import Profile from './profile';
import UserService from '../services/user-service';
import PopupFrame from './popup-frame';
import Tray from './tray';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import DateInput from './date-input';
import SelectedDate from './selected-date';
import SettingsButton from './settings-button';
import moment from 'moment';
import TimesheetEntry from './timesheet-entry';
import SlidingPanel from './sliding-panel';
import Create from './create';
import { RequireNetwork } from './router';
import Splash from './splash';
import Offline from './offline';
import UserValidation from './user-validation';

const styles = StyleSheet.create({
  container: {

  },
  header: {
    paddingLeft: 15,
    height: 60,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(214,214,214)',
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowRadius: 6,
    zIndex: 1,
    backgroundColor: Platform.OS === 'electron' ? 'transparent' : '#3ec4ff'
  },
  toolbar: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingHorizontal: 15
  },
  body: {
    flex: 1,
    backgroundColor: 'white'
  },
  footer: {
    paddingLeft: 15,
    height: Platform.OS === 'electron' ? 40 : 60,
    borderTopWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(214,214,214)',
    alignItems: 'center',
    flexDirection: 'row',
    shadowOffset: {
      width: 0,
      height: -2
    },
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowRadius: 6,
    zIndex: 1,
    backgroundColor: Platform.OS === 'electron' ? 'transparent' : '#3ec4ff'
  },
  dateInput: {
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: 'rgb(237,231,192)'
  },
  footerButtonText: {
    fontSize: Platform.OS === 'electron' ? 11 : 16,
    color: Platform.OS === 'electron' ? 'rgb(118,118,118)' : 'white',
    marginLeft: 10,
    alignSelf: 'center'
  },
  createPanel: {
    backgroundColor: '#f5fbff',
    height: 200
  }
});

export default class Home extends React.Component {
  static childContextTypes = {
    timeService: PropTypes.object.isRequired
  };

  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    windowService: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      date: null,
      trayHighlightMode: 'never',
      times: [],
      showCreate: false
    };
  }

  getChildContext() {
    const { serviceManager } = this.context;

    return {
      timeService: serviceManager.timeService
    };
  }

  componentWillMount() {
    const { windowService, userService, timeService } = this.context.serviceManager;
    this.onChangeDate(null, false);
    this.interval = setInterval(() => this.onChangeDate(), 5000);
    timeService.on('running', this.onChangeDate);

    if(Platform.OS === 'electron') {
      windowService.get('home').on('hide', this.onHideWindow);
    }
  }

  componentWillUnmount() {
    const { windowService, userService, timeService } = this.context.serviceManager;
    clearInterval(this.interval);

    const { date: oldDate, times: oldTimes } = this.state;

    if(oldDate) {
      const oldRow = oldTimes.find(x => x.date.toISOString() === oldDate.toISOString());
      oldRow.removeListener('update', this.onChangeTimeList);
    }

    timeService.removeListener('running', this.onChangeDate);

    if(Platform.OS === 'electron') {
      windowService.get('home').removeListener('hide', this.onHideWindow);
    }


  }

  onChangeDate = async (date = this.state.date, forceUpdate = true) => {
    const { timeService } = this.context.serviceManager;
    if(date === null) {
      date = moment(timeService.getCurrentDate()).startOf('day').toDate();
    }

    const { date: oldDate, times: oldTimes } = this.state;
    if(oldDate && oldDate.getTime() !== date.getTime()) {
      const oldRow = oldTimes.find(x => x.date.toISOString() === oldDate.toISOString());
      oldRow.removeListener('update', this.onChangeTimeList);
    }

    const times = await timeService.list(date);

    const row = times.find(x => x.date.toISOString() === date.toISOString());
    if(!oldDate || oldDate.getTime() !== date.getTime()) {
      row.on('update', this.onChangeTimeList);
    }

    const running = await timeService.getRunning();
    this.setState({ date, times, running });
    //if(forceUpdate) this.forceUpdate();
  }

  onHideWindow = () => {
    this.setState({ trayHighlightMode: 'never' });
  };

  onChangeTimeList = () => {
    this.onChangeDate();
  };

  onPressSettings = (event) => {
    const { history, windowService } = this.context;

    if(windowService.platform === 'web' && windowService.get('settings').isVisible()) {
      windowService.get('settings').close();
    } else {
      history.push('/settings', { window: 'settings', popup: event.target });
    }
  };

  onPressTray = (trayBounds) => {
    const { history } = this.context;

    const window = windowService.get('home');
    if(window.isVisible()) {
      this.setState({ trayHighlightMode: 'never' });
      window.hide();
    } else {
      this.setState({ trayHighlightMode: 'always' });
      history.replace('/', { window: 'home', popup: trayBounds });
    }
  }

  onPressCreate = () => {
    const { windowService, history } = this.context;
    const { createButton } = this.refs;

    if(Platform.OS === 'electron') {
      const window = windowService.get('create');
      if(window.isVisible()) {
        window.hide();
      } else {
        history.push('/create', { window: 'create', popup: createButton });
      }
    } else {
      //LayoutAnimation.easeInEaseOut();
      this.setState({ showCreate: !this.state.showCreate });
      this.create.blur();
    }
  };

  onPressStart = () => {
    if(Platform.OS === 'electron') {

    } else {
      //LayoutAnimation.easeInEaseOut();
      this.setState({ showCreate: false });
    }
  };

  onPressTimesheetEntry = async (time) => {
    const { timeService } = this.context.serviceManager;
    if(time.running) {
      await timeService.stopTime(time);
    } else {
      await timeService.startTime(time);
    }
    await this.onChangeDate();
  };

  renderOffline(online, captive) {
    const { trayHighlightMode } = this.state;

    return (
      <View style={{flex: 1}}>
        {Platform.OS === 'electron' && <Tray onPress={this.onPressTray} highlightMode={trayHighlightMode}/>}
        <Offline logo={Platform.OS !== 'electron'} online={online} captive={captive}/>
      </View>
    );
  }

  render() {
    const { userService } = this.context.serviceManager;
    const { style } = this.props;
    const { date, trayHighlightMode, running, showCreate } = this.state;
    const rows = this.state.times.find(row => moment(row.date).startOf('day').isSame(date));
    const times = rows ? rows.times || [] : []

    return (
      <PopupFrame backgroundColor="rgb(237,237,237)" borderColor="rgb(145,145,145)" style={StyleSheet.absoluteFill}>
        <RequireNetwork splash={() => <Splash/>} offline={(online, captive) => this.renderOffline(online, captive)}>
          <View style={StyleSheet.absoluteFill}>
            <View style={{flex: 1}}>
              {!userService.user.validated && <UserValidation/>}
              <View style={styles.header}>
                <SelectedDate date={date}/>
                <Tray running={running} onPress={this.onPressTray} highlightMode={trayHighlightMode}/>
                <View style={styles.toolbar}>
                  <SettingsButton size={Platform.OS === 'electron' ? 16 : 20} color={Platform.OS === 'electron' ? 'rgb(118,118,118)' : 'white'}/>
                </View>
              </View>
              <View style={styles.body}>
                <DateInput value={date} onChange={this.onChangeDate} times={this.state.times} style={styles.dateInput}/>
                <ScrollView contentContainerStyle={{ paddingBottom: 8 }}>
                  {times.map((time) => <TimesheetEntry key={time.localId} time={time} onPress={() => this.onPressTimesheetEntry(time)}/>)}
                </ScrollView>
              </View>
              <SlidingPanel component={Create} childRef={(ref) => this.create = ref} show={showCreate} openedStyle={{height: 200}} onBlur={this.onPressStart} style={styles.createPanel} onPressStart={this.onPressStart}>
                <View style={styles.footer}>
                  <Button onPress={this.onPressCreate}>
                    <Icon ref="createButton" size={Platform.OS === 'electron' ? 16 : 30} color={Platform.OS === 'electron' ? 'rgb(118,118,118)' : 'white'} name="plus"/>
                    <Text style={styles.footerButtonText}>Create New Entry</Text>
                  </Button>
                </View>
              </SlidingPanel>
            </View>
          </View>
        </RequireNetwork>
      </PopupFrame>
    );
  }
}
