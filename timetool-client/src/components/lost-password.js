
import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import Form from './form';
import { RequireNetwork } from './router';
import Splash from './splash';
import Offline from './offline';

const styles = StyleSheet.create({

});

export default class LostPassword extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      error: null
    };
  }

  onPressResetPassword = async () => {
    const { serviceManager, history } = this.context;
    const { userService } = serviceManager;

    const { email } = this.state;

    try {
      this.setState({ error: null });
      await userService.lostPassword(email);
      history.replace('/signin', { animated: 'slide-right', next: '/', message: 'We\'ve sent you an email to reset your password.' });
    } catch(err) {
      console.info(err);
      this.setState({ error: err.message });
      return;
    }
  };

  onPressSignIn = () => {
    const { history } = this.context;
    const { next = '/' } = history.location.state || {};

    history.replace('/signin', { animated: 'slide-right', next });
  };

  onChangeEmail = (email) => {
    email = email.trim().replace(/\s+/, '');
    this.setState({ email });
  };

  render() {
    const  { email, password, error } = this.state;
    return (
      <RequireNetwork splash={() => <Splash/>} offline={(online, captive) => <Offline online={online} captive={captive}/>}>
        <Form
          error={error}
          fields={{
            email: {
              value: email,
              placeholder: 'Email',
              onChangeText: this.onChangeEmail,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'email-address'
            }
          }}
          buttonTitle="Reset Password"
          onSubmit={this.onPressResetPassword}
          footer={{
            cancel: {
              text: 'Remembered your password?',
              title: 'Sign in',
              onPress: this.onPressSignIn
            }
          }}/>
      </RequireNetwork>
    );
  }
}
