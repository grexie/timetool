
import React from 'react';
import {
  Animated,
  Dimensions,
  View,
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';

export default class AnimateChildren extends React.Component {
  constructor() {
    super();
    this.state = {
      previousChildren: null,
      animation: new Animated.Value(0)
    };
  }

  componentWillReceiveProps(nextProps) {
    const { animation } = this.state;
    const { animated, onDone } = nextProps;

    if(animated) {
      this.setState((state) => {
        state.previousChildren = this.props.children;

        animation.setValue(0);
        Animated.timing(animation, {
          toValue: 1,
        }).start(() => {
          this.setState({ previousChildren: null });
          if(onDone) {
            onDone();
          }
        });
      });
    } else {
      animation.setValue(1);
      if(onDone) {
        onDone();
      }
    }
  }

  render() {
    const { previousChildren, animation } = this.state;
    const { children: nextChildren, style, containerStyle } = this.props;

    let { animated } = this.props;
    if(animated === true) {
      animated = 'slide-left';
    }

    const device = Dimensions.get('window');

    const previousTransforms = [];
    const nextTransforms = [];

    if(animated === 'slide-left') {
      previousTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -device.width ]
        })
      });

      nextTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ device.width, 0 ]
        })
      });
    }

    if(animated === 'slide-right') {
      previousTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, device.width ]
        })
      });

      nextTransforms.push({
        translateX: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ -device.width, 0 ]
        })
      });
    }

    if(animated === 'slide-up') {
      previousTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, -device.height ]
        })
      });

      nextTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ device.height, 0 ]
        })
      });
    }

    if(animated === 'slide-down') {
      previousTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ 0, device.height ]
        })
      });

      nextTransforms.push({
        translateY: animation.interpolate({
          inputRange: [ 0, 1 ],
          outputRange: [ -device.height, 0 ]
        })
      });
    }

    return (
      <View style={containerStyle}>
        {previousChildren && <Animated.View style={[StyleSheet.absoluteFill, style, { transform: previousTransforms }]}>
          {previousChildren}
        </Animated.View>}
        <Animated.View style={[StyleSheet.absoluteFill, style, { transform: nextTransforms }]}>
          {nextChildren}
        </Animated.View>
      </View>
    )
  }
}
