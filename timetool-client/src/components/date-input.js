import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Button from './button';
import UserService from '../services/user-service';
import AnimateChildren from './animate-children';
import { formatHours } from '../utils';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgb(252,248,230)',
    paddingVertical: 5
  },
  dateInputButton: {

  },
  dateInputButtonView: {
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center'
  },
  dateInputButtonImage: {
    width: 20,
    height: 20
  },
  dateInput: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'stretch',
    flex: 1,
    height: 60
  },
  dateInputDay: {
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  dateInputDayView: {
    alignItems: 'center',
    width: 36,
    height: 36,
    justifyContent: 'center',
    borderRadius: 22.5
  },
  dateInputDayViewSelected: {
    backgroundColor: '#ffc40c',
  },
  dateInputDayTitle: {
    color: 'black',
    opacity: 0.6,
    fontSize: 12,
    fontWeight: '300'
  },
  dateInputDayValue: {
    color: 'black',
    opacity: 0.6,
    fontWeight: '300',
    fontSize: 12
  },
  dateInputDayValueSelected: {
    color: 'white',
    opacity: 1,
    fontWeight: '400'
  }
});

export default class DateInput extends React.Component {

  static contextTypes = {
    timeService: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.animated = false;
    this.previousStartOfWeek = null;
  }

  componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    const { previousStartOfWeek } = this;
    const { timeService } = this.context;

    const startOfWeek = moment(value).startOf(timeService.getMomentWeekStyle());

    if(previousStartOfWeek === null) {
      this.animated = false;
    } else if(moment(startOfWeek).isAfter(previousStartOfWeek)) {
      this.animated = 'slide-left';
    } else if(moment(startOfWeek).isBefore(previousStartOfWeek)) {
      this.animated = 'slide-right';
    }

    this.previousStartOfWeek = startOfWeek;
  }

  shouldComponentUpdate(nextProps, nextState) {
    for(let i = 0; i < 7; i++) {
      const row1 = this.props.times[i];
      const row2 = nextProps.times[i];

      if(!row1 || !row2) {
        return true;
      }

      if(formatHours(row1.total) !== formatHours(row2.total)) {
        return true;
      }
    }

    return !moment(nextProps.value).isSame(this.props.value);
  }

  render() {
    const { times = [], value, onChange, style } = this.props;
    const { timeService } = this.context;
    const { animated } = this;

    // use week when start of week is sunday
    const startOfWeek = moment(value).startOf(timeService.getMomentWeekStyle());
    const weekdays = Array(7).fill(null).map((x, i) => moment(startOfWeek).add(i, 'days').toDate());
    const lastWeek = moment(value).subtract(7, 'days').toDate();
    const nextWeek = moment(value).add(7, 'days').toDate();

    return (
      <View style={[styles.container, style]}>
        <Button onPress={onChange && (() => onChange(lastWeek))} style={styles.dateInputButtonView}>
          <Icon name="arrow-left" size={18}/>
        </Button>
        <AnimateChildren style={styles.dateInput} containerStyle={styles.dateInput} animated={animated} onDone={() => this.animated = false}>
          {weekdays.map((date, i) => (
            <TouchableOpacity style={styles.dateInput} key={i} onPress={onChange && (() => onChange(date))}>
              <View style={styles.dateInputDay}>
                <Text style={styles.dateInputDayTitle}>{moment(date).format('ddd')}</Text>
                <View style={[styles.dateInputDayView, moment(date).isSame(value, 'day') ? styles.dateInputDayViewSelected : null]}>
                  <Text style={[styles.dateInputDayValue, moment(date).isSame(value, 'day') ? styles.dateInputDayValueSelected : null]}>{formatHours(times[i] ? times[i].total : 0)}</Text>
                </View>
              </View>
            </TouchableOpacity>
          ))}
        </AnimateChildren>
        <Button onPress={onChange && (() => onChange(nextWeek))} style={styles.dateInputButtonView}>
          <Icon name="arrow-right" size={18}/>
        </Button>
      </View>
    );
  }
};
