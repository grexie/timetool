
import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import moment from 'moment';
import { formatHours } from '../../utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignSelf: 'center',
    marginHorizontal: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderRadius: 5
  },
  text: {
    color: '#3ec4ff',
    fontSize: 20
  }
});

export default class Tray extends React.Component {



  render() {
    const { running } = this.props;

    const duration = running ? formatHours(running.duration) : '--:--';

    return (
      <View style={styles.container}>
        <Text style={styles.text}>{duration}</Text>
      </View>
    );
  }
}
