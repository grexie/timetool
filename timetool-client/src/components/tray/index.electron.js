
import React from 'react';
import {
  View
} from 'react-native';
import PropTypes from 'prop-types';
import Logo from '../logo';
import Profile from '../profile';
import moment from 'moment';
import { remote } from 'electron';
const { nativeImage, Tray: NativeTray } = remote;
import { roundedRect, roundedPolygon, formatHours } from '../../utils';

const WIDTH = 55;
const HEIGHT = 18;

const createTrayImage = (running, highlight, scaleFactor) => {
  const canvas = document.createElement('canvas');
  canvas.width = WIDTH * scaleFactor;
  canvas.height = HEIGHT * scaleFactor;

  const ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle = highlight ? 'rgb(71,159,251)' : 'rgba(0,0,0,1)';
  roundedRect(ctx, 0, 0, canvas.width, canvas.height, 2 * scaleFactor);
  ctx.fill();
  ctx.font = `${14 * devicePixelRatio}px "BlinkMacSystemFont"`;
  ctx.textAlign = 'end';
  ctx.globalCompositeOperation = 'destination-out';

  ctx.beginPath();
  if(running) {
    const padding = 3;
    const width = 11;
    roundedPolygon(ctx, [
      { x: (1 + padding) * scaleFactor, y: padding * scaleFactor },
      { x: (1 + padding + width) * scaleFactor, y: HEIGHT / 2 * scaleFactor },
      { x: (1 + padding) * scaleFactor, y: (HEIGHT - padding) * scaleFactor }
    ], 2);
  } else {
    const padding = 4;
    const width = 10;
    roundedPolygon(ctx, [
      { x: padding * scaleFactor, y: padding * scaleFactor },
      { x: (padding + width) * scaleFactor, y: padding * scaleFactor },
      { x: (padding + width) * scaleFactor, y: (HEIGHT - padding) * scaleFactor },
      { x: padding * scaleFactor, y: (HEIGHT - padding) * scaleFactor }
    ], 2);
  }
  ctx.fill();

  const text = running ? formatHours(running.duration) : '--:--';
  ctx.fillText(`${text}`, (WIDTH - 4) * scaleFactor, (HEIGHT - 4) * scaleFactor);

  const dataURL = canvas.toDataURL('image/png');

  const image = nativeImage.createEmpty();
  image.addRepresentation({
    width: canvas.width,
    height: canvas.height,
    scaleFactor,
    dataURL
  });
  if(!highlight) {
    image.setTemplateImage(true);
  }
  return image;
};


export default class Tray extends React.Component {
  componentWillMount() {
    const { running, onPress, highlightMode } = this.props;
    const image = createTrayImage(running, highlightMode === 'always', devicePixelRatio);
    this.tray = new NativeTray(image);
    this.tray.setHighlightMode('never');

    window.addEventListener('beforeunload', () => this.tray.destroy(), false);

    this.tray.on('click', (event, trayBounds) => {
      if(onPress) {
        onPress(trayBounds);
      }
    });

    remote.app.dock.hide();
  }

  componentWillReceiveProps(nextProps) {
    const image = createTrayImage(nextProps.running, nextProps.highlightMode === 'always', devicePixelRatio);
    this.tray.setImage(image);
  }

  componentWillUnmount() {
    this.tray.destroy();
    remote.app.dock.show();
  }

  render() {
    const { style } = this.props;

    return (
      <View/>
    );
  }
}
