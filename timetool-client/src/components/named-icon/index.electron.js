
import React from 'react';
import {
  View,
  Image
} from 'react-native';
import PropTypes from 'prop-types';

const icons = {
  NSPreferencesGeneral: require('../../assets/images/NSPreferencesGeneral.png'),
  NSUser: require('../../assets/images/NSUser.png')
};

export default class NamedIcon extends React.Component {
  render() {
    const { name, size } = this.props;

    return (
      <View style={{width: size, height: size}}>
        <Image source={icons[name]} resizeMode="contain" style={{width: size, height: size}}/>
      </View>
    )
  }

};
