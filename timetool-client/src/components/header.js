
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity
} from 'react-native';
import Button from './button';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  header: {
    paddingLeft: 25,
    paddingRight: 15,
    height: 60,
    backgroundColor: '#3ec4ff',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  headerTitle: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  titleImage: {
    marginRight: 15
  },
  titleText: {
    fontFamily: 'System',
    fontSize: 20,
    fontWeight: '500',
    lineHeight: 22,
    color: 'white'
  },
  closeButtonContainer: {

  },
  closeButtonView: {
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  closeButtonImage: {
    width: 20,
    height: 20
  }
});

export default class Header extends React.Component {
  render() {
    const { icon, text, style, onPressClose } = this.props;

    return (
      <View style={[styles.header, style]}>
        <View style={styles.headerTitle}>
          {icon && <Icon name={icon} size={20} color="white" style={styles.titleImage}/>}
          <Text style={styles.titleText}>{text}</Text>
        </View>
        <Button style={styles.closeButtonView} onPress={onPressClose}>
          <Icon name="close" size={20} color="white"/>
        </Button>
      </View>
    );
  }
}
