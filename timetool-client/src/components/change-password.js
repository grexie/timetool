
import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TextInput,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Form from './form';
import { RequireNetwork } from './router';
import Splash from './splash';
import Offline from './offline';
import Header from './header';

const styles = StyleSheet.create({

});

export default class LostPassword extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      lostPassword: false,
      current: '',
      password: '',
      confirm: '',
      error: null
    };
  }

  onPressChangePassword = async () => {
    const { serviceManager, history } = this.context;
    const { userService } = serviceManager;

    const { current, password, confirm } = this.state;

    if(!password || password !== confirm) {
      this.setState({ error: 'New passwords do not match'});
      return;
    }

    try {
      this.setState({ error: null, message: null });
      await userService.changePassword(current, password);

      history.replace('/settings', { animated: Platform.OS !== 'electron' ? 'slide-right' : null, window: 'settings', close: true });
    } catch(err) {
      this.setState({ error: err.message });
      return;
    }
  };

  onPressLostPassword = async () => {
    const { serviceManager, history } = this.context;
    const { userService } = serviceManager;

    const { email } = userService.user;

    try {
      this.setState({ error: null });
      await userService.lostPassword(email);
      this.setState({ lostPassword: true });
    } catch(err) {
      console.info(err);
      this.setState({ error: err.message });
      return;
    }
  };

  onChangeCurrent = (current) => {
    this.setState({ current });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  onChangeConfirm = (confirm) => {
    this.setState({ confirm });
  };

  onPressClose = () => {
    const { history } = this.context;
    history.replace('/settings', { animated: Platform.OS !== 'electron' ? 'slide-right' : null, window: 'settings', close: true });
  };

  renderHeader() {
    return <Header text="Change password" onPressClose={this.onPressClose}/>
  }

  render() {
    const  { lostPassword, current, password, confirm, error } = this.state;
    return (
      <RequireNetwork splash={() => <Splash/>} offline={(online, captive) => <Offline online={online} captive={captive}/>}>
        <Form
          header={Platform.OS === 'electron' ? null : (() => this.renderHeader())}
          error={error}
          message={lostPassword && 'We\'ve sent you an email to reset your password.'}
          fields={{
            current: {
              value: current,
              placeholder: 'Current Password',
              onChangeText: this.onChangeCurrent,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'default',
              secureTextEntry: true
            },
            password: {
              value: password,
              placeholder: 'Password',
              onChangeText: this.onChangePassword,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'default',
              secureTextEntry: true
            },
            confirm: {
              value: confirm,
              placeholder: 'Confirm Password',
              onChangeText: this.onChangeConfirm,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'default',
              secureTextEntry: true
            }
          }}
          buttonTitle="Change Password"
          onSubmit={this.onPressChangePassword}
          footer={{
            lostPassword: {
              text: 'Forgotten your password?',
              title: 'Reset it now',
              onPress: this.onPressLostPassword
            }
          }}/>
      </RequireNetwork>
    );
  }
}
