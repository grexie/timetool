
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TextInput,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Button from './button';
import Config from '../config';
import Profile from './profile';
import PopupFrame from './popup-frame';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 10,
    flexShrink: 0
  },
  textInput: {
    backgroundColor: 'white',
    fontSize: 13,
    flex: 1,
    borderRadius: 3,
    borderColor: 'rgb(214,214,214)',
    borderWidth: 1,
    borderStyle: 'solid',
    paddingVertical: 10,
    paddingHorizontal: Platform.OS === 'electron' ? 5 : 10,
    textAlignVertical: 'top'
  },
  startButton: {
    backgroundColor: '#3ec4ff',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowRadius: 0,
    alignSelf: 'flex-end',
    paddingHorizontal: 15,
    paddingVertical: 4,
    borderRadius: 3,
    marginTop: 8
  },
  startButtonActive: {

  },
  startButtonText: {
    fontSize: 13,
    color: 'white'
  }
});

export default class Create extends React.Component {

  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  componentWillMount() {

  }

  componentWillUnmount() {

  }

  onChangeText = (text) => {
    text = text.replace(/\n|\r\n/g, '');
    this.setState({ text });
  };

  onPressStart = async () => {
    this.submitting = true;
    const { timeService } = this.context.serviceManager;
    let { text } = this.state;
    text = text.trim().replace(/\s+/g, ' ');

    if(!text) {
      return;
    }
    this.blur();

    if(this.props.onPressStart) this.props.onPressStart();
    if(Platform.OS === 'electron') window.close();

    await timeService.create(text);
    await timeService.list();
  }

  blur() {
    this.refs.textInput.blur();
  }

  render() {
    const { text } = this.state;
    const { style, onBlur } = this.props;

    return (
      <PopupFrame backgroundColor="rgb(237,237,237)" borderColor="rgb(145,145,145)" style={[styles.container, style]}>
        <TextInput ref="textInput" underlineColorAndroid="transparent" onBlur={() => !this.submitting && onBlur && onBlur()} blurOnSubmit={true} onSubmitEditing={() => this.onPressStart()} returnKeyType="done" style={styles.textInput} multiline={true} numberOfLines={4} placeholder="What are you working on now?" keyboardType="default" value={text} onChangeText={this.onChangeText}/>
        <Button onPress={this.onPressStart} style={styles.startButton}><Text style={styles.startButtonText}>Start</Text></Button>
      </PopupFrame>
    );
  }
}
