
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';


const styles = StyleSheet.create({
  selectedDate: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    flexGrow: 1
  },
  selectedDateLabel: {
    fontSize: Platform.OS === 'electron' ? 16 : 20,
    fontWeight: '600',
    lineHeight: Platform.OS === 'electron' ? 24 : 28,
    color: Platform.OS === 'electron' ? 'black' : 'white'
  },
  selectedDateFull: {
    fontSize: Platform.OS === 'electron' ? 14 : 16,
    fontWeight: '300',
    lineHeight: Platform.OS === 'electron' ? 18 : 22,
    color: Platform.OS === 'electron' ? 'black' : 'white'
  }
});

export default class SelectedDate extends React.Component {
  static contextTypes = {
    timeService: PropTypes.object.isRequired
  };

  render() {
    const { date } = this.props;
    const { timeService } = this.context;

    let dayLabel = moment(date).calendar(timeService.getCurrentDate(), {
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      nextWeek: 'dddd',
      lastDay: '[Yesterday]',
      lastWeek: '[Last] dddd',
      sameElse: '[]'
    });
    if(!dayLabel) {
      dayLabel = moment(date).fromNow();
    }
    const fullDate = moment(date).format('ddd, D MMM YYYY');

    return (
      <View style={styles.selectedDate}>
        <Text style={styles.selectedDateLabel}>{dayLabel}</Text>
        <Text style={styles.selectedDateFull}>{fullDate}</Text>
      </View>
    );
  }
};
