
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';

import logoImage from '../assets/images/grexie-logo-white.png';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#3ec4ff'
  },
  logo: {
    width: 78,
    height: 78,
    marginRight: 8
  },
  text: {
    fontFamily: 'Open Sans',
    fontSize: 20,
    color: 'white'
  },
  titleFont: {
    fontWeight: '600',
    letterSpacing: 0.6
  },
  companyFont: {
    opacity: 0.8
  },
  lightFont: {
    fontWeight: '100'
  },
  boldFont: {
    fontWeight: '600'
  }
});

export default class Logo extends React.Component {
  render() {
    const { style } = this.props;

    return (
      <View style={[styles.container, style]}>
        <Image source={logoImage} style={styles.logo}/>
        <View style={styles.textContainer}>
          <Text style={styles.text}>
            <Text style={styles.titleFont}>Timetool</Text>{'\n'}
            <Text style={styles.companyFont}><Text style={styles.lightFont}>by</Text> <Text style={styles.boldFont}>grexie</Text></Text>
          </Text>
        </View>
      </View>
    );
  }
}
