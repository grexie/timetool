
import React from 'react';

import {
  View
} from 'react-native';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-native';

class RequireUser extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      user: null,
      resolved: false
    };
  }

  componentWillMount() {
    const { userService } = this.context.serviceManager;

    userService.on('update', this.onUpdateUser);
    if(userService.ready) {
      this.onUpdateUser(userService.user);
    }
  }

  componentWillUnmount() {
    const { userService } = this.context.serviceManager;
    console.info(this.context.serviceManager);
    userService.removeListener('update', this.onUpdateUser);
  }

  onUpdateUser = (user) => {
    this.setState({ user, resolved: true });
  }

  render() {
    const { user, resolved } = this.state;
    const { splash, component: Component, ...props } = this.props;

    if(!resolved) {
      return splash ? splash() : <View/>;
    }

    return user ? (
      <Component {...props}/>
    ) : (
      <Redirect to={{
        pathname: '/signup',
        state: { animated: false, next: '/', window: 'signin', close: true }
      }}/>
    );
  }
}

export class PrivateRoute extends React.Component {
  render() {
    const { splash, component, ...rest } = this.props;

    return (
      <Route {...rest} render={props => <RequireUser splash={splash} component={component} {...props}/>}/>
    );
  }
};

export class RequireNetwork extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      ready: false,
      isOnline: null,
      isCaptivePortal: null
    };
  }

  componentWillMount() {
    const { serviceManager } = this.context;
    const { networkService } = serviceManager;

    if(networkService.ready) {
      this.onReady();
    } else {
      networkService.once('ready', this.onReady);
    }
  }

  componentWillUnmount() {
    const { serviceManager } = this.context;
    const networkService = serviceManager;

    networkService.removeListener('ready', this.onReady);
    networkService.removeListener('update', this.onUpdate);
  }

  onReady = () => {
    const { serviceManager } = this.context;
    const { networkService } = serviceManager;

    networkService.on('update', this.onUpdate);
    this.setState({ ready: true, isOnline: networkService.isOnline, isCaptivePortal: networkService.isCaptivePortal });
  };

  onUpdate = () => {
    const { serviceManager } = this.context;
    const { networkService } = serviceManager;

    this.setState({ isOnline: networkService.isOnline, isCaptivePortal: networkService.isCaptivePortal });
  };

  render() {
    const { ready, isOnline, isCaptivePortal } = this.state;
    const { splash, offline } = this.props;
    
    if(!ready) {
      return splash ? splash() : <View/>;
    }

    if(!isOnline || isCaptivePortal) {
      return offline ? offline(isOnline, isCaptivePortal) : splash ? splash() : <View/>;
    }

    const child = React.Children.only(this.props.children);
    return child;
  }
}

export class RequireService extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      ready: false
    };
  }

  componentWillMount() {
    const { serviceManager } = this.context;
    const service = serviceManager[this.props.service];

    if(service.ready) {
      this.onReady();
    } else {
      service.once('ready', this.onReady);
    }
  }

  componentWillUnmount() {
    const { serviceManager } = this.context;
    const service = serviceManager[this.props.service];

    service.removeListener('ready', this.onReady);
  }

  onReady = () => {
    this.setState({ ready: true });
  };

  render() {
    const { ready } = this.state;
    const { splash } = this.props;

    if(!ready) {
      return splash ? splash() : <View/>;
    }

    const child = React.Children.only(this.props.children);
    return child;
  }
}
