
import React from 'react';

import {
  View
} from 'react-native';
import PropTypes from 'prop-types';
import { Router as ReactRouter, Route, Redirect, Switch, Link } from 'react-router-native';
import { createBrowserHistory } from 'history';
import { RequireNetwork, PrivateRoute, RequireService } from './utils.js'

import ServiceManager from '../../services';

export { RequireNetwork, PrivateRoute, RequireService, Route, Redirect, Switch, Link };

const withHistory = (Application) => class RequireHistory extends React.Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  };

  static childContextTypes = {
    history: PropTypes.object.isRequired
  };

  getChildContext() {
    return {
      history: this.context.router.history
    };
  }

  render() {
    return <Application {...this.props}/>
  }
};

export class Router extends React.Component {
  static createComponent = (application) => () => <Router application={application}/>;

  static childContextTypes = {
    serviceManager: PropTypes.instanceOf(ServiceManager).isRequired,
    windowService: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.serviceManager = new ServiceManager();
    Object.assign(window, this.serviceManager);
    this.history = createBrowserHistory();
  }

  getChildContext() {
    return {
      serviceManager: this.serviceManager,
      windowService: this.serviceManager.windowService
    };
  }

  render() {
    const { application: Application } = this.props;

    return (
      <ReactRouter history={this.history}>
        <View style={{flex: 1}}>
          <Route component={withHistory(Application)}/>
        </View>
      </ReactRouter>
    );
  }

}
