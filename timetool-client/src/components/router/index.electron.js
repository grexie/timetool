
import React from 'react';
import {
  Platform,
  UIManager,
  findNodeHandle,
  View
} from 'react-native';
import PropTypes from 'prop-types';
import { Router as ReactRouter, Route, Redirect, Switch, Link } from 'react-router';
import { createBrowserHistory } from 'history';
import { ipcRenderer, remote } from 'electron';
const { getCurrentWindow } = remote;

import { RequireNetwork, PrivateRoute, RequireService } from './utils';
import ServiceManager from '../../services';
console.info(ServiceManager);

export { RequireNetwork, PrivateRoute, RequireService, Route, Redirect, Switch, Link };

export class Router extends React.Component {
  static createComponent = (application) => () => <Router application={application}/>;

  static childContextTypes = {
    serviceManager: PropTypes.instanceOf(ServiceManager).isRequired,
    windowService: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.serviceManager = new ServiceManager();
    this.history = this.createPatchedHistory();
    ipcRenderer.on('history.push', (event, pathname, state) => this.history.push(pathname, state));
    ipcRenderer.on('history.replace', (event, pathname, state) => this.history.replace(pathname, state));
  }

  getUserConfirmation = (message, callback, nextOp, nextLocation, nextState) => {
    const { window } = nextState || {};
    if(window) {
      const { windowService } = this.serviceManager;

      // check if it's the same window we're currently in
      const isCurrentWindow = windowService.get(window).getNativeWindowHandle().equals(getCurrentWindow().getNativeWindowHandle());
      if(!isCurrentWindow) {
        if(nextOp === 'push') {
          windowService.push(nextLocation, nextState);
        } else if(nextOp === 'replace') {
          windowService.replace(nextLocation, nextState);
        }
      }

      if(nextState.popup) {
        const { width, height } = windowService.get(window).getBounds();
        const { popup } = nextState;
        const bounds = {
          x: parseInt(popup.x + popup.width / 2 - width / 2, 10),
          y: parseInt(popup.y + popup.height, 10),
          width,
          height
        };

        windowService.get(window).setBounds(bounds);
        if(popup.parent) {
          windowService.get(window).setParentWindow(getCurrentWindow());
        }
      } else if(nextState.close && !isCurrentWindow) {
        getCurrentWindow().close();
      }

      if(nextState.showWindow !== false) {
        windowService.get(window).show();
      } else if(nextState.showWindow === false) {
        windowService.get(window).hide();
      }

      callback(isCurrentWindow);


    } else {
      callback(true);
    }
  };

  getChildContext() {
    return {
      serviceManager: this.serviceManager,
      windowService: this.serviceManager.windowService,
      history: this.history
    };
  }

  createPatchedHistory() {
    let initialLocation = '/_blank', nextOp, nextLocation, nextState;

    const history = createBrowserHistory({
      initialEntries: ['/_blank'],
      initialIndex: 0,
      keyLength: 6,
      getUserConfirmation: (message, callback) => this.getUserConfirmation(message, callback, nextOp, nextLocation, nextState)
    });

    const transformPopup = (state, callback) => {
      if(!state || !state.popup) {
        return callback();
      }
      if(state.popup instanceof React.Component) {
        const handle = findNodeHandle(state.popup);
        UIManager.measureInWindow(handle, (x, y, width, height) => {
          const currentWindow = remote.getCurrentWindow();
          state.popup = {
            x: x + currentWindow.getBounds().x,
            y: y + currentWindow.getBounds().y,
            width,
            height,
            parent: true
          };
          callback();
        });
      } else {
        callback();
      }
    };

    const push = history.push.bind(history), replace = history.replace.bind(history);
    history.push = (location, state) => {
      if(typeof location === 'object') {
        state = location.state;
        location = location.pathname;
      }
      nextOp = 'push';
      nextLocation = location;
      nextState = state;
      transformPopup(nextState, () => push(location, state));
    };
    history.replace = (location, state) => {
      if(typeof location === 'object') {
        state = location.state;
        location = location.pathname;
      }
      nextOp = 'replace';
      nextLocation = location;
      nextState = state;
      transformPopup(nextState, () => replace(location, state));
    };

    history.block('_');

    return history;
  };

  render() {
    const { application: Application } = this.props;

    return (
      <ReactRouter history={this.history}>
        <View style={{flex: 1}}>
          <Route exact path="/_blank" render={() => <View/>}/>
          <Route component={Application}/>
        </View>
      </ReactRouter>
    );
  }


};
