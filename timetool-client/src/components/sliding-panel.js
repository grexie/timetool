
import React from 'react';
import {
  View,
  KeyboardAvoidingView,
  StyleSheet,
  Platform
} from 'react-native';

export default class SlidingPanel extends React.Component {

  render() {
    const { component: Component, show, openedStyle, childRef, ...props } = this.props;

    if(Platform.OS === 'ios' && KeyboardAvoidingView) {
      return (
        <KeyboardAvoidingView keyboardVerticalOffset={-20} behavior="position" style={show ? openedStyle : {flexShrink: 0, position: 'relative'}} contentContainerStyle={{flexGrow: 1}}>
          {this.props.children}
          <View style={show ? {flex: 1} : { height: 0 }}>
            <Component ref={childRef} {...props}/>
          </View>
        </KeyboardAvoidingView>
      );
    } else {
      return (
        <View style={show ? [openedStyle] : {flexShrink: 0, position: 'relative'}}>
          {this.props.children}
          <View style={show ? {flex: 1} : { height: 0 }}>
            <Component ref={childRef} {...props}/>
          </View>
        </View>
      );
    }

  }
}
