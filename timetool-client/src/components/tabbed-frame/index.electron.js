
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Button from '../button';
import NamedIcon from '../named-icon';
import { remote } from 'electron';

const styles = StyleSheet.create({
  container: {

  },
  titleBar: {
    position: 'relative'
  },
  body: {
    flexGrow: 1,
    backgroundColor: 'rgb(236,236,236)'
  },
  titleBarText: {
    textAlign: 'center',
    fontSize: 13,
    color: 'rgb(59,58,59)',
    marginVertical: 3
  },
  titleBarTextInactive: {
    color: 'rgb(172,172,172)'
  },
  tabButtons: {
    flexDirection: 'row',
    paddingHorizontal: 4,
    paddingTop: 3
  },
  tabButton: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 3,
    paddingTop: 4,
    paddingHorizontal: 2,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    overflow: 'hidden'
  },
  tabButtonText: {
    color: 'rgb(22,20,22)',
    fontSize: 11,
    paddingHorizontal: 3,
    paddingVertical: 3
  },
  tabButtonTextInactive: {
    color: 'rgb(155,154,154)'
  }
});



export default class TabbedFrame extends React.Component {

  static contextTypes = {
    windowService: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      active: remote.getCurrentWindow().isFocused(),
      tabIndex: 0
    };
  }

  componentWillMount() {
    remote.getCurrentWindow().on('focus', this.onWindowFocus);
    remote.getCurrentWindow().on('blur', this.onWindowBlur);
  }

  componentWillUnmount() {
    remote.getCurrentWindow().removeListener('focus', this.onWindowFocus);
    remote.getCurrentWindow().removeListener('blur', this.onWindowBlur);
  }

  onWindowFocus = () => {
    this.setState({ active: remote.getCurrentWindow().isFocused() });
  };

  onWindowBlur = () => {
    this.setState({ active: remote.getCurrentWindow().isFocused() });
  };

  render() {
    const { children, triangleHeight = 16 } = this.props;
    const { active, tabIndex } = this.state;
    const bounds = remote.getCurrentWindow().getBounds();
    const { scaleFactor: devicePixelRatio } = remote.screen.getDisplayMatching(bounds);

    const tabs = React.Children.toArray(this.props.children).filter(x => x.type === TabbedFrame.Tab);
    const tabButtons = tabs.map((tab, i) => (
      <Button key={i} style={styles.tabButton} onPress={() => this.setState({ tabIndex: i })}>
        {tabIndex === i && active && <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, background: 'linear-gradient(rgb(203,202,203), rgb(188,187,188))', borderBottom: '1px solid rgb(182,182,182)' }}/>}
        {tabIndex === i && !active && <div style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, background: 'rgb(232,232,232)', borderBottom: '1px solid rgb(218,218,218)' }}/>}
        <div style={{ position: 'absolute', WebkitAppRegion: 'no-drag', top: 0, left: 0, right: 0, bottom: 0, background: 'transparent' }}/>
        <NamedIcon name={tab.props.iconNamed} size={32}/>
        <Text style={[styles.tabButtonText, !active ? styles.tabButtonTextInactive : null]}>{tab.props.title}</Text>
      </Button>
    ));

    return (
      <View style={[StyleSheet.absoluteFill, styles.container]}>
        <View>
          {active && <div style={{ position: 'absolute', WebkitAppRegion: 'drag', top: 0, left: 0, right: 0, bottom: 0, background: 'linear-gradient(rgb(232,231,232), rgb(209,208,209))', borderBottom: '1px solid rgb(182,182,182)' }}/>}
          {!active && <div style={{ position: 'absolute', WebkitAppRegion: 'drag', top: 0, left: 0, right: 0, bottom: 0, background: 'rgb(243,243,243)', borderBottom: '1px solid rgb(218,218,218)' }}/>}
          <View style={styles.titleBar}>
            <Text style={[styles.titleBarText, !active ? styles.titleBarTextInactive : null]}>General</Text>
          </View>
          <View style={styles.tabButtons}>
            {tabButtons}
          </View>
        </View>
        <View style={styles.body}>
          {tabs[tabIndex]}
        </View>
      </View>
    );
  }
}


TabbedFrame.Tab = class Tab extends React.Component {

  render() {
    return (
      <View style={[{flex: 1}, this.props.style]}>
        {this.props.children}
      </View>
    );
  }
}
