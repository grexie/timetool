
import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  ScrollView
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,

  },
  contentContainer: {
    flexGrow: 1,
    alignItems: 'stretch',
    backgroundColor: '#f5fbff'
  },
  tab: {
    alignItems: 'stretch',
    flexShrink: 0,
    paddingHorizontal: 25,
    marginVertical: 20
  },
  header: {
    marginBottom: 20,
  },
  headerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginBottom: 20
  },
  headerTitleText: {
    fontSize: 18,
    fontWeight: '300',
    color: '#95989a'
  },
});

export default class TabbedFrame extends React.Component {
  render() {
    const { header, footer } = this.props;

    return (
      <View style={styles.container}>
        {header && header()}
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={{flex: 1}}>
            {this.props.children}
          </View>
          {footer && footer()}
        </ScrollView>
      </View>
    );
  }
};

TabbedFrame.Tab = class Tab extends React.Component {
  render() {
    return (
      <View style={styles.tab}>
        <View style={styles.header}>
          <Text style={styles.headerTitleText}>{this.props.title}</Text>
        </View>
        {this.props.children}
      </View>
    );
  }
};
