
import React from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  StyleSheet
} from 'react-native';
import PropTypes from 'prop-types';
import Config from '../config';
import TabbedFrame from './tabbed-frame';
import NamedIcon from './named-icon';
import Profile from './profile';
import Logo from './logo';
import Header from './header';
import Button from './button';

const styles = StyleSheet.create({
  preferenceRow: {
    flexDirection: Platform.OS === 'electron' ? 'row' : null,
    alignItems: Platform.OS === 'electron' ? 'center' : null
  },
  generalTab: {
    padding: 20,
    flex: 1
  },
  accountTab: {
    flex: 1,
    padding: 20,
    alignItems: 'center',
    justifyContent: 'center'
  },
  footer: {
    paddingVertical: 25,
    paddingHorizontal: 25
  },
  segmented: {
    alignSelf: Platform.OS === 'electron' ? 'center' : 'stretch',
    borderRadius: 5,
    borderColor: '#3ec4ff',
    borderWidth: 1,
    borderStyle: 'solid',
    overflow: 'hidden'
  },
  infoText: {
    color: Platform.OS === 'electron' ? 'black' : '#666',
    fontSize: Platform.OS === 'electron' ? 13 : 15,
    textAlign: Platform.OS === 'electron' ? 'right' : 'left',
    width: Platform.OS === 'electron' ? 250 : null,
    marginRight: Platform.OS === 'electron' ? 10 : null,
    marginBottom: Platform.OS === 'electron' ? 0 : 5
  },
  button: {
    marginLeft: -1,
    flex: 1,
    alignSelf: 'stretch',
    backgroundColor: 'transparent',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: Platform.OS === 'electron' ? 4 : 8,
    paddingHorizontal: 10,
    borderLeftWidth: 1,
    borderColor: '#3ec4ff',
    borderStyle: 'solid'
  },
  buttonActive: {
    backgroundColor: '#3ec4ff'
  },
  buttonText: {
    color: '#3ec4ff',
    fontSize: Platform.OS === 'electron' ? 13 : 15,
    fontWeight: '300'
  },
  buttonTextActive: {
    color: 'white',
    fontWeight: '400'
  }
});

export default class Settings extends React.Component {

  static childContextTypes = {
    userService: PropTypes.object.isRequired
  };

  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  getChildContext() {
    const { userService } = this.context.serviceManager;
    return {
      userService
    };
  }

  onPressClose = () => {
    const { history } = this.context;
    history.replace('/', { animated: 'slide-right' });
  }

  onPressWeekStarts = async (day) => {
    const { userService } = this.context.serviceManager;
    const { user } = userService;

    user.weekStarts = day;
    this.setState({ user });
    await userService.putUser(user);
  }

  renderHeader() {
    return <Header text="Settings" icon="settings" onPressClose={this.onPressClose}/>;
  }

  renderFooter() {
    return <Logo style={styles.footer}/>;
  }

  render() {
    const { userService } = this.context.serviceManager;
    const { user } = userService;

    return (
      <TabbedFrame header={() => this.renderHeader()}>
        <TabbedFrame.Tab title="General" iconNamed="NSPreferencesGeneral" style={styles.generalTab}>
          <View style={styles.preferenceRow}>
            <View><Text style={styles.infoText}>First day of the week:</Text></View>
            <Button.Segmented style={styles.segmented}>
              <Button pressed={user.weekStarts === 'monday'} style={styles.button} activeStyle={styles.buttonActive} onPress={() => this.onPressWeekStarts('monday')} textStyle={styles.buttonText} textActiveStyle={styles.buttonTextActive}>Monday</Button>
              <Button pressed={user.weekStarts === 'sunday'} style={styles.button} activeStyle={styles.buttonActive} onPress={() => this.onPressWeekStarts('sunday')} textStyle={styles.buttonText} textActiveStyle={styles.buttonTextActive}>Sunday</Button>
            </Button.Segmented>
          </View>
        </TabbedFrame.Tab>
        <TabbedFrame.Tab title="Account" iconNamed="NSUser" style={styles.accountTab}>
          <Profile/>
        </TabbedFrame.Tab>
      </TabbedFrame>
    );
  }
}
