
import React from 'react';

import {
  View,
  UIManager,
  findNodeHandle
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Button from '../button';

import { remote } from 'electron';

const { Menu, getCurrentWindow } = remote;

export default class SettingsButton extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  onPress = (event) => {
    const menu = Menu.buildFromTemplate([
      { label: 'Preferences...', click: this.onPressSettings },
      { type: 'separator' },
      { label: 'Quit', accelerator: 'CmdOrCtrl+Q', click: this.onPressQuit }
    ]);

    const handle = findNodeHandle(this.refs.button);
    UIManager.measureInWindow(handle, (x, y, width, height) => {
      menu.popup(getCurrentWindow(), {
        x: x - 5,
        y: y + height + 5,
        async: true
      });
    });
  }

  onPressSettings = () => {
    const { history } = this.context;

    getCurrentWindow().hide();
    history.push('/settings', { window: 'settings' });
  };

  onPressQuit = () => {
    const { windowService } = this.context.serviceManager;
    windowService.quit();
  };

  render() {
    const { size, color } = this.props;

    return (
      <Button ref="button" onPress={this.onPress}>
        <Icon size={size} color={color} name="settings"/>
      </Button>
    );
  }
}
