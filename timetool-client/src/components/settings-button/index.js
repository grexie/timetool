
import React from 'react';

import {
  View,
  UIManager,
  findNodeHandle
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import Button from '../button';

export default class SettingsButton extends React.Component {
  static contextTypes = {
    history: PropTypes.object.isRequired
  };

  onPress = () => {
    const { history } = this.context;

    history.push('/settings', { animated: 'slide-left' });
  };

  render() {
    const { size, color } = this.props;

    return (
      <Button ref="button" onPress={this.onPress}>
        <Icon size={size} color={color} name="settings"/>
      </Button>
    );
  }
}
