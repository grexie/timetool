
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import Button from './button';


const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgb(252,248,230)',
    borderColor: 'rgb(237,231,192)',
    borderWidth: 1,
    borderStyle: 'solid',
    paddingVertical: 4,
    paddingHorizontal: 8,
    margin: 10,
    borderRadius: 3,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowColor: 'rgba(0,0,0,0.2)',
    shadowRadius: 6
  },
  text: {
    fontSize: Platform.OS !== 'electron' ? 15 : 13,
    fontWeight: '300',
    lineHeight: Platform.OS !== 'electron' ? 20 : 17
  },
  button: {
    position: 'absolute',
    right: 8,
    bottom: 4
  },
  buttonText: {
    textAlign: 'right',
    fontWeight: '600',
    color: '#666',
    fontSize: Platform.OS !== 'electron' ? 15 : 13,
    lineHeight: Platform.OS !== 'electron' ? 20 : 17
  }
});

export default class UserValidation extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      showResend: true
    };
  }

  onPressResend = async () => {
    const { userService } = this.context.serviceManager;

    this.setState({ showResend: false });
    try {
      await userService.sendValidationEmail();
    } catch(err) {
      console.error(err);
    }
    this.timeout = setTimeout(() => this.setState({ showResend: true }), 30000);
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    const { style } = this.props;
    const { showResend } = this.state;

    return (
      <View style={[styles.container, style]}>
        <Text style={styles.text}>
          We{"'"}ve sent you a message to validate your account. Please click
          on the link in the email to validate the email address linked to your
          account.
        </Text>
        {showResend && <Button style={styles.button} onPress={this.onPressResend}>
          <Text style={styles.buttonText}>Resend Email</Text>
        </Button>}
      </View>
    );
  }
}
