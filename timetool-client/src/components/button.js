
import React from 'react';

import {
  TouchableOpacity,
  View,
  StyleSheet,
  Text,
  TouchableWithoutFeedback
} from 'react-native';

const styles = StyleSheet.create({
  buttonView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  buttonText: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  segmentedContainer: {
    alignSelf: 'stretch',
    flexDirection: 'row',
    alignItems: 'stretch'
  },
  segmentedView: {
    flex: 1,

    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default class Button extends React.Component {
  constructor() {
    super();
    this.state = {
      pressed: false
    };
  }

  onPressIn = () => {
    this.setState({ pressed: true });
  }

  onPressOut = () => {
    this.setState({ pressed: false });
  }

  render() {
    const { pressed: overridePressed = null, style, activeStyle, textStyle, textActiveStyle, onPress, onLayout } = this.props;
    const { pressed } = this.state;

    return (
      <TouchableWithoutFeedback
        onPress={onPress}
        onPressIn={this.onPressIn}
        onPressOut={this.onPressOut} onLayout={onLayout}>
        <View style={[styles.buttonView, style, (overridePressed === null ? pressed : overridePressed) ? activeStyle : null]}>
          {!textStyle && this.props.children}
          {textStyle && <Text style={[styles.buttonText, textStyle, (overridePressed === null ? pressed : overridePressed) ? textActiveStyle : null]}>{this.props.children}</Text>}
        </View>
      </TouchableWithoutFeedback>
    );
  }
};

Button.Segmented = class Segmented extends React.Component {
  render() {
    const buttons = React.Children.map(this.props.children, (button, i) => (
      <View key={i} style={styles.segmentedView}>
        {button}
      </View>
    ));

    return (
      <View style={[styles.segmentedContainer, this.props.style]}>
        {buttons}
      </View>
    );
  }
};
