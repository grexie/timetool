
import React from 'react';
import {
  View,
  StyleSheet
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default class PopupFrame extends React.Component {
  render() {
    const { style } = this.props;

    return (
      <View style={[styles.container, style]}>
        {this.props.children}
      </View>
    );
  }
}
