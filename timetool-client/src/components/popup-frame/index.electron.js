
import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import { roundedRect, roundedPolygon } from '../../utils';
import { remote } from 'electron';
const styles = StyleSheet.create({
  container: {
    padding: 12
  }
});

export default class PopupFrame extends React.Component {

  static contextTypes = {
    serviceManager: PropTypes.object.isRequired
  };

  componentWillMount() {
    window.addEventListener('resize', this.onResize, false);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onResize, false);
  }

  onResize = () => {
    const { serviceManager } = this.context;
    requestAnimationFrame(() => {
      if(this.canvas) {
        const bounds = remote.getCurrentWindow().getBounds();
        this.canvas.width = bounds.width * devicePixelRatio;
        this.canvas.height = bounds.height * devicePixelRatio;
        this.onDraw(this.canvas, devicePixelRatio);
      }
    });
  }

  onDraw = (canvas, devicePixelRatio) => {
    const { backgroundColor = '#fefefe', borderColor } = this.props;
    const { triangleWidth = 16, triangleHeight = 16 } = this.props;
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = backgroundColor;
    ctx.strokeStyle = borderColor;
    ctx.shadowColor = 'rgba(0,0,0,0.5)';
    ctx.shadowBlur = 12 * devicePixelRatio;
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 4 * devicePixelRatio;

    const dpr = devicePixelRatio;
    const padding = 12 * dpr;
    const paddingTop = (triangleHeight + 2) * dpr;
    const radius = 5 * dpr;

    ctx.beginPath();
    roundedPolygon(ctx, [
      { x: padding, y: paddingTop },
      { x: canvas.width / 2 - triangleWidth * dpr, y: paddingTop, radius: 15 * dpr },
      { x: canvas.width / 2, y: paddingTop - triangleHeight * dpr, radius: 3 * dpr },
      { x: canvas.width / 2 + triangleWidth * dpr, y: paddingTop, radius: 15 * dpr },
      { x: canvas.width - padding, y: paddingTop },
      { x: canvas.width - padding, y: canvas.height - padding },
      { x: padding, y: canvas.height - padding }
    ], radius);
    ctx.fill();

    ctx.beginPath();
    roundedPolygon(ctx, [
      { x: padding - 0.5, y: paddingTop - 0.5 },
      { x: canvas.width / 2 - triangleWidth * dpr, y: paddingTop, radius: 15 * dpr },
      { x: canvas.width / 2, y: paddingTop - triangleHeight * dpr, radius: 3 * dpr },
      { x: canvas.width / 2 + triangleWidth * dpr, y: paddingTop, radius: 15 * dpr },
      { x: canvas.width - padding + 0.5, y: paddingTop - 0.5 },
      { x: canvas.width - padding + 0.5, y: canvas.height - padding + 0.5 },
      { x: padding - 0.5, y: canvas.height - padding + 0.5 }
    ], radius);
    ctx.shadowColor = 'transparent';
    ctx.stroke();
  };

  render() {
    const { children, triangleHeight = 16, style } = this.props;
    const bounds = remote.getCurrentWindow().getBounds();
    const { scaleFactor: devicePixelRatio } = remote.screen.getDisplayMatching(bounds);

    const styles = {position: 'absolute', zIndex: -1, top: 0, left: 0, right: 0, bottom: 0, width: '100%', height: '100%'};

    return (
      <View style={[StyleSheet.absoluteFill, styles.container]}>
        <canvas ref={(canvas) => canvas && (this.canvas = canvas) && this.onDraw(canvas, devicePixelRatio)} width={bounds.width * devicePixelRatio} height={bounds.height * devicePixelRatio} style={styles}></canvas>
        <View style={[{ borderRadius: 5, position: 'absolute', left: 12, right: 12, bottom: 12, top: triangleHeight + 2, overflow: 'hidden' }]}>
          <View style={style}>
            {children}
          </View>
        </View>
      </View>
    );
  }
}
