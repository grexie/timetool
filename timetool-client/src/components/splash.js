
import React from 'react';

import Logo from './logo';

export default class Splash extends React.Component {
  render() {
    return (
      <Logo style={{flex: 1, justifyContent: 'center'}}/>
    );
  }
}
