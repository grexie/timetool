
import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TextInput
} from 'react-native';
import PropTypes from 'prop-types';
import Form from './form';
import { RequireNetwork } from './router';
import Splash from './splash';
import Offline from './offline';

const styles = StyleSheet.create({

});

export default class SignIn extends React.Component {
  static contextTypes = {
    serviceManager: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  };

  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      error: null
    };
  }

  onPressSignIn = async () => {
    const { serviceManager, history } = this.context;
    const { userService } = serviceManager;

    const { email, password } = this.state;

    try {
      this.setState({ error: null });
      await userService.authenticate(email, password);
      history.replace('/', { animated: 'slide-down', window: 'home', showWindow: false, close: true });
    } catch(err) {
      this.setState({ error: err.message });
      return;
    }
  };

  onPressSignUp = () => {
    const { history } = this.context;
    const { next = '/' } = history.location.state || {};

    history.replace('/signup', { animated: 'slide-right', next });
  };

  onPressLostPassword = () => {
    const { history } = this.context;
    const { next = '/signin' } = history.location.state || {};

    history.replace('/lost-password', { animated: 'slide-left', window: 'signin', next });
  };

  onChangeEmail = (email) => {
    email = email.trim().replace(/\s+/, '');
    this.setState({ email });
  };

  onChangePassword = (password) => {
    this.setState({ password });
  };

  render() {
    const  { email, password, error } = this.state;
    const { history } = this.context;
    const { message } = history.location.state || {};

    return (
      <RequireNetwork splash={() => <Splash/>} offline={(online, captive) => <Offline online={online} captive={captive}/>}>
        <Form
          error={error}
          message={message}
          fields={{
            email: {
              value: email,
              placeholder: 'Email',
              onChangeText: this.onChangeEmail,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'email-address'
            },
            password: {
              value: password,
              placeholder: 'Password',
              onChangeText: this.onChangePassword,
              autoCapitalize: 'none',
              autoCorrect: false,
              keyboardType: 'default',
              secureTextEntry: true
            }
          }}
          buttonTitle="Sign In"
          onSubmit={this.onPressSignIn}
          footer={{
            lostPassword: {
              text: 'Forgotten your password?',
              title: 'Reset it now',
              onPress: this.onPressLostPassword
            },
            signIn: {
              text: 'Not got an account?',
              title: 'Sign up',
              onPress: this.onPressSignUp
            }
          }}/>
      </RequireNetwork>
    );
  }
}
