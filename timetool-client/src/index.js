
import './platforms';
import React from 'react';
import {
  Button,
  Image,
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform
} from 'react-native';
import PropTypes from 'prop-types';
import { RequireNetwork, RequireService, Router, Route, PrivateRoute } from './components/router';
import AnimateRoutes from './components/animate-routes';
import Logo from './components/logo';

import SignIn from './components/sign-in';
import SignUp from './components/sign-up';
import LostPassword from './components/lost-password';
import ChangePassword from './components/change-password';
import Home from './components/home';
import Settings from './components/settings';
import Create from './components/create';
import Splash from './components/splash';
import Offline from './components/offline';

import Config from './config';




export default class Application extends React.Component {

  static contextTypes = {
    router: PropTypes.any.isRequired
  };

  constructor() {
    super();
  }

  render() {

    return (
      <View style={StyleSheet.absoluteFill}>
        <StatusBar
          backgroundColor="#3ec4ff"
          barStyle="light-content"/>
        <RequireService splash={() => <Splash/>} service="timeService">
          <View style={[StyleSheet.absoluteFill, Platform.OS !== 'electron' ? styles.mobileContainer : null, Platform.OS === 'ios' ? styles.iosContainer : null]}>
            <AnimateRoutes>
              <PrivateRoute exact splash={() => <Splash/>} path="/" component={Home}/>
              <PrivateRoute exact splash={() => <Splash/>} path="/settings" component={Settings}/>
              <PrivateRoute exact splash={() => <Splash/>} path="/create" component={Create}/>
              <Route exact path="/signin" component={SignIn}/>
              <Route exact path="/signup" component={SignUp}/>
              <Route exact path="/lost-password" component={LostPassword}/>
              <Route exact path="/change-password" component={ChangePassword}/>
            </AnimateRoutes>
          </View>
        </RequireService>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'stretch'
  },
  splash: {
    flex: 1,
    justifyContent: 'center',
    borderBottomWidth: 0
  },
  iosContainer: {
    paddingTop: 20
  },
  mobileContainer: {
    backgroundColor: '#3ec4ff'
  }
});


AppRegistry.registerComponent('Timetool', () => Router.createComponent(Application));

if (Platform.OS === 'electron' || Platform.OS === 'web') {
  const rootTag = document.getElementById('react-root')
  AppRegistry.runApplication('Timetool', { rootTag });
  window.addEventListener('beforeunload', () => {
    AppRegistry.unmountApplicationComponentAtRootTag(rootTag);
  });
}
