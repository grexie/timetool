const storage = require('electron-json-storage');

export default class AsyncStorage {
  static getItem = (key) => new Promise((resolve, reject) => {
    storage.get(key, (err, data) => {
      if(err) {
        reject(err);
        return;
      }

      resolve(data.value);
    })
  });

  static setItem = (key, value) => new Promise((resolve, reject) => {
    storage.set(key, { value }, (err) => {
      if(err) {
        reject(err);
        return;
      }

      resolve();
    })
  });

  static removeItem = (key) => new Promise((resolve, reject) => {
    storage.remove(key, (err) => {
      if(err) {
        reject(err);
        return;
      }

      resolve();
    })
  });
}
