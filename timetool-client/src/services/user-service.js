
import AsyncStorage from './async-storage/index';
import { EventEmitter } from 'events';
import Config from '../config';

const UserServiceTable = new WeakMap();

export default class UserService extends EventEmitter {
  constructor(serviceManager) {
    super();
    UserServiceTable.set(this, { user: null, ready: false });
    this.getUser().catch(() => {}).then(() => {
      UserServiceTable.get(this).ready = true;
      this.emit('ready');
    });

    serviceManager.once('ready', () => {
      const { networkService, websocketService } = serviceManager;

      websocketService.on('update.user', (user) => {
        UserServiceTable.set(this, { user });
        this.emit('update', user);
      });
    });
  }

  async create(name, email, password) {
    const response = await fetch(`${Config.URL_PREFIX}/user/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        name,
        email,
        password
      })
    });
    if(!response.ok) {
      throw new Error("The supplied email address is already registered.");
    }
    const user = await response.json();
    await AsyncStorage.setItem('@Timetool:accessToken', JSON.stringify(user.accessToken));
    delete user.accessToken;
    Object.assign(UserServiceTable.get(this), { user });
    this.emit('update', user);
    return user;
  }

  async authenticate(email, password) {
    const response = await fetch(`${Config.URL_PREFIX}/user/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password
      })
    });
    if(!response.ok) {
      throw new Error("Email and password combination not known");
    }
    const user = await response.json();
    await AsyncStorage.setItem('@Timetool:accessToken', JSON.stringify(user.accessToken));
    delete user.accessToken;
    Object.assign(UserServiceTable.get(this), { user });
    this.emit('update', user);
    return user;
  }

  async signOut() {
    this.user.pushToken = null;
    await this.putUser(this.user, true);
    UserServiceTable.get(this).user = null;
    await AsyncStorage.removeItem('@Timetool:accessToken');
    this.emit('update', null);
  }

  async getAccessToken() {
    const accessToken = await AsyncStorage.getItem('@Timetool:accessToken');
    if(accessToken) return JSON.parse(accessToken);
    return accessToken;
  }

  async getUser() {
    if(this.user) {
      return this.user;
    }

    const accessToken = await this.getAccessToken();
    if(!accessToken) {
      Object.assign(UserServiceTable.get(this), { user: null });
      this.emit('update', null);
      return;
    }

    const response = await fetch(`${Config.URL_PREFIX}/user/me`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    });
    const user = await response.json();
    Object.assign(UserServiceTable.get(this), { user });
    this.emit('update', user);
    return user;
  }

  async putUser(user, suppressUpdate = false) {
    const accessToken = await this.getAccessToken();
    const response = await fetch(`${Config.URL_PREFIX}/user/me`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify(user)
    });
    const updatedUser = await response.json();
    Object.assign(UserServiceTable.get(this), { user: updatedUser });
    if(!suppressUpdate) this.emit('update', updatedUser);
    return updatedUser;
  }

  async sendValidationEmail() {
    const accessToken = await this.getAccessToken();
    const response = await fetch(`${Config.URL_PREFIX}/user/me/validate`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({

      })
    });
    if(!response.ok) {
      throw new Error("Already valid");
    }
    return true;
  }

  async lostPassword(email) {
    const response = await fetch(`${Config.URL_PREFIX}/user/lost-password`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email
      })
    });
    if(!response.ok) {
      throw new Error("No user found with that email address");
    }
    return true;
  }

  async changePassword(current, password) {
    const accessToken = await this.getAccessToken();
    const response = await fetch(`${Config.URL_PREFIX}/user/me/change-password`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        current, password
      })
    });
    if(!response.ok) {
      const { error } = await response.json();
      throw new Error(error);
    }
    return true;
  }

  get user() {
    return UserServiceTable.get(this).user;
  }

  get ready() {
    return UserServiceTable.get(this).ready;
  }

  subscribe(listener) {
    this.on('update', listener);
    if(this.user) {
      listener(user);
    }
    return () => this.removeListener('update', listener);
  }
};
