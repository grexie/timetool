
import { EventEmitter } from 'events';
import { remote } from 'electron';

// target the main electron process
if(WEBPACK_TARGET === 'electron') {
  const _require = require;
  module.exports = _require('./index.js');

// otherwise target electron renderer
} else {

  class ServiceManager extends EventEmitter {
    constructor() {
      super();

      const serviceManager = remote.getGlobal('serviceManager');
      Object.assign(window, serviceManager);
      Object.assign(this, serviceManager);
    }
  }

  module.exports = ServiceManager;
}
