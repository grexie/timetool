
import { EventEmitter } from 'events';
import Config from '../config';

const WebSocketServiceTable = new WeakMap();

const closeSocket = (service) => {
  if(WebSocketServiceTable.get(service).webSocket) {
    const ws = WebSocketServiceTable.get(service).webSocket;
    delete WebSocketServiceTable.get(service).webSocket;
    ws.close();
  }
}

const createSocket = (service, accessToken) => {
  closeSocket(service);
  const webSocket = new WebSocket(`${Config.WEBSOCKET_ADDRESS}?u=${encodeURIComponent(accessToken)}`);
  let interval;

  const onOpen = () => {
    interval = setInterval(() => {
      webSocket.send(JSON.stringify({ type: 'keepalive' }));
    }, 10000);
  };

  const onClose = () => {
    clearInterval(interval);
    setTimeout(() => {
      if(WebSocketServiceTable.get(service).webSocket) {
        createSocket(service, accessToken);
      }
    }, 1000);
  };

  const onError = () => {
  };

  const onMessage = ({ type, body }) => {
    service.emit(type, body);
  };

  if('onopen' in webSocket) {
    webSocket.onopen = () => onOpen();
    webSocket.onclose = (event) => onClose(event);
    webSocket.onerror = (event) => onError(event);
    webSocket.onmessage = (event) => onMessage(JSON.parse(event.data));
  } else if(webSocket.on) {
    webSocket.on('open', () => onOpen());
    webSocket.on('close', (event) => onClose(event));
    webSocket.on('error', (event) => onError(event));
    webSocket.on('message', (message) => onMessage(JSON.parse(message)));
  }

  WebSocketServiceTable.get(service).webSocket = webSocket;
};

export default class WebSocketService extends EventEmitter {
  constructor(serviceManager) {
    super();

    WebSocketServiceTable.set(this, { serviceManager });

    serviceManager.once('ready', () => {
      const { userService } = serviceManager;

      let _id = null;
      const onUpdateUser = async (user) => {
        const { id } = user || { id: null };
        if(_id === id) {
          return;
        }
        _id = id;

        if(id) {
          const accessToken = await userService.getAccessToken();
          createSocket(this, accessToken);
        } else {
          closeSocket(this);
        }
      }
      userService.on('update', (user) => onUpdateUser(user));
      if(userService.ready) onUpdateUser(userService.user);
    });
  }

  shutdown() {
    closeSocket(this);
  }
};
