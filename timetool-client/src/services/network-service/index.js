

import { changeNetworkStatus, NetworkServiceBase } from './base';
import { NetInfo } from 'react-native';


const setupReactNative = (service, options) => {
  const onChange = (reach) => {
    const status = reach.toLowerCase() === 'none' ? 'offline' : 'online';
    changeNetworkStatus(service, status);
  };
  NetInfo.addEventListener('change', onChange);
};

export default class NetworkService extends NetworkServiceBase {
  constructor(serviceManager, options) {
    super(serviceManager, options);

    setupReactNative(this, options);
  }
};
