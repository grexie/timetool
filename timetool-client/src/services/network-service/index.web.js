
import { changeNetworkStatus, NetworkServiceBase } from './base';

const setupBrowser = (service, options) => {
  changeNetworkStatus(service, 'online');
};

export default class NetworkService extends NetworkServiceBase {
  constructor(serviceManager, options) {
    super(serviceManager, options);

    setupBrowser(this, options);
  }
};
