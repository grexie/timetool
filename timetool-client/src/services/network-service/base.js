
import { EventEmitter } from 'events';
import url from 'url';
import Config from '../../config';
import { uuid } from '../utils';
const NetworkServiceTable = new WeakMap();

export const changeNetworkStatus = async (service, status) => {
  try {
    const options = NetworkServiceTable.get(service);

    if(options.status === status) {
      if(status === 'online') {
        await checkCaptivePortal(service);
      }

      return;
    }

    NetworkServiceTable.get(service).status = status;
    if(status === 'online') {
      await checkCaptivePortal(service);
      service.emit('update', service);
    } else {
      NetworkServiceTable.get(service).isCaptivePortal = false;
      service.emit('update', service);
    }
  } finally {
    if(!NetworkServiceTable.get(service).ready) {
      NetworkServiceTable.get(service).ready = true;
      service.emit('ready');
    }
  }
};


export const checkCaptivePortal = async (service) => {
  if(NetworkServiceTable.get(service).status !== 'online') {
    NetworkServiceTable.get(service).isCaptivePortal = false;
    return;
  }

  try {
    const echo = uuid()
    const response = await fetch(`${Config.URL_PREFIX}/captive-portal/${echo}`, { method: 'GET' });
    if(!response.ok) throw "Service not ok";
    const json = await response.json();

    if(json.status !== 'success') {
      throw "Status not successful";
    }

    if(new Date(json.timestamp).getTime() < new Date().getTime() - 300*1000) {
      throw `Captive portal out of date ${json.timestamp}`;
    }

    if(echo !== json.echo) {
      throw `Captive portal not returned echo ${echo}`;
    }

    if(NetworkServiceTable.get(service).isCaptivePortal !== false) {
      NetworkServiceTable.get(service).isCaptivePortal = false;
      service.emit('update', service);
    }
    // check again in 1 minute
    NetworkServiceTable.get(service).captiveTimeout = setTimeout(() => checkCaptivePortal(service), 60000);
  } catch(err) {
    console.info(err);
    if(NetworkServiceTable.get(service).isCaptivePortal !== true) {
      NetworkServiceTable.get(service).isCaptivePortal = true;
      service.emit('update', service);
    }
    // check again in 5 seconds
    NetworkServiceTable.get(service).captiveTimeout = setTimeout(() => checkCaptivePortal(service), 5000);
  } finally {
    if(!NetworkServiceTable.get(service).ready) {
      NetworkServiceTable.get(service).ready = true;
      service.emit('ready');
    }
  }
};


export class NetworkServiceBase extends EventEmitter {
  constructor(serviceManager, options) {
    super();

    NetworkServiceTable.set(this, {
      ready: false,
      status: null,
      isCaptivePortal: null,
      captiveTimeout: null
    });
  }

  get ready() {
    const { ready } = NetworkServiceTable.get(this);
    return ready;
  }

  get isOnline() {
    const { status } = NetworkServiceTable.get(this);
    return status === 'online';
  }

  get isCaptivePortal() {
    const { isCaptivePortal } = NetworkServiceTable.get(this);
    return isCaptivePortal;
  }

  shutdown() {
    clearTimeout(NetworkServiceTable.get(this).captiveTimeout);
  }
};
