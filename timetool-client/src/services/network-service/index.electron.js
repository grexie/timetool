
import { changeNetworkStatus, NetworkServiceBase } from './base';
import { BrowserWindow, ipcMain } from 'electron';
import url from 'url';


const setupElectron = (service, { port }) => {
  const window = new BrowserWindow({ width: 0, height: 0, show: false });
  window.loadURL(url.format({
    pathname: '/network-status.html',
    hostname: 'localhost',
    port,
    protocol: 'http:',
    slashes: true
  }));
  ipcMain.on('network-status.update', (event, status) => {
    changeNetworkStatus(service, status);
  });
};


export default class NetworkService extends NetworkServiceBase {
  constructor(serviceManager, options) {
    super(serviceManager, options);

    setupElectron(this, options);
  }
};
