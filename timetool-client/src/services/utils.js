
import _uuid from 'uuid';
import baseX from 'base-x';

const base16 = baseX('0123456789abcdef');
const base62 = baseX('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

export const uuid = () => {
  const uuid = _uuid().toString().replace(/[^\w]/g, '');
  return base62.encode(base16.decode(uuid));
};

export const timeout = (delay, promise) => Promise.race([
  promise,
  new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('timeout')), delay);
  })
]);
