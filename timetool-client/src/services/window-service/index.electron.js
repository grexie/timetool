
import { app, BrowserWindow, nativeImage } from 'electron';
import url from 'url';
import fs from 'fs';
import path from 'path';

const windows = {};

let isClosing = false;

const WindowServiceTable = new WeakMap();

export default class WindowService {

  constructor(serviceManager, options) {
    WindowServiceTable.set(this, { serviceManager, options });
    this.create(require('./home.json'));
    this.create(require('./signin.json'));
    this.create(require('./settings.json'));
    this.create(require('./create.json'));

    app.on('before-quit', () => {
      isClosing = true;
      console.info('before-quit');
      for(const v in windows) {
        const window = windows[v];
        window.removeAllListeners('close');
        if(!window.isDestroyed()) window.close();
      }
      serviceManager.shutdown();
      app.exit();
    });

  }

  create(options) {
    const { port } = WindowServiceTable.get(this).options;
    const { name, url: _url, devTools, ...props } = options;

    if(process.env.NODE_ENV !== 'production') {
      props.webPreferences = props.webPreferences || {};
      props.webPreferences.webSecurity = false;
    }
    props.show = false;

    if(!windows[name]) {
      const wnd = new BrowserWindow(props);
      if(devTools) {
        wnd.webContents.openDevTools();
      }

      wnd.loadURL(url.format({
        pathname: _url || '/_blank',
        hostname: 'localhost',
        port,
        protocol: 'http:',
        slashes: true
      }));
      windows[name] = wnd;

      windows[name].on('close', (event) => {
        if(!isClosing) {
          event.preventDefault();
          windows[name].hide();
          this.replace('/_blank', { window: name });
        }
      });
    }
  }

  push(pathname, { window: name, ...state }) {
    const window = this.get(name);
    if(!window) {
      throw new Error(`Window with name ${name} does not exist`);
    }

    window.webContents.send('history.push', pathname, state);
  }

  replace(pathname, { window: name, ...state }) {
    const window = this.get(name);
    if(!window) {
      throw new Error(`Window with name ${name} does not exist`);
    }

    window.webContents.send('history.replace', pathname, state);
  }

  get(name) {
    return windows[name];
  }
  
  quit() {
    app.quit();
  }

};
