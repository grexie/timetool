
import moment from 'moment';
import Config from '../config';
import { EventEmitter } from 'events';
import { timeout, uuid } from './utils';

const TimeTable = new WeakMap();
const TimeListTable = new WeakMap();
const TimeServiceTable = new WeakMap();

class Time extends EventEmitter {
  constructor(service, json) {
    super();
    TimeServiceTable.set(this, service);
    TimeTable.set(this, json);
  }

  get localId() {
    const { localId, id } = TimeTable.get(this);
    return localId || id || null;
  }

  get id() {
    const { id } = TimeTable.get(this);
    return id;
  }

  get description() {
    const { description } = TimeTable.get(this);
    return description;
  }

  get created() {
    const { created } = TimeTable.get(this);
    return new Date(created);
  }

  get deleted() {
    const { deleted } = TimeTable.get(this);
    return deleted && new Date(deleted);
  }

  get started() {
    const { started } = TimeTable.get(this);
    return new Date(started);
  }

  get ended() {
    const { ended } = TimeTable.get(this);
    if(ended) return new Date(ended);
    return ended;
  }

  get duration() {
    const { started, ended, duration } = TimeTable.get(this);
    const timeService = TimeServiceTable.get(this);

    // get the current duration even if this time is currently running
    if(ended === null) {
      const currentDuration = (timeService.getCurrentDate().getTime() - new Date(started).getTime()) / (3600*1000);
      return duration + currentDuration;
    }

    return duration;
  }

  get running() {
    const { running } = TimeTable.get(this);
    return running;
  }

  toJSON() {
    return Object.assign({}, TimeTable.get(this));
  }
}

class TimeList extends EventEmitter {
  constructor(service, timeList = [], date) {
    super();
    const comparator = (a, b) => {
      if(a.created.getTime() < b.created.getTime()) return 1;
      if(a.created.getTime() > b.created.getTime()) return -1;
      return 0;
    };
    TimeListTable.set(this, { service, timeList, comparator, date });
  }

  sort(comparator) {
    TimeListTable.set(this).comparator = comparator;
  }

  get date() {
    const { date } = TimeListTable.get(this);
    return new Date(date);
  }

  get total() {
    return this.times.reduce((a, b) => a + b.duration, 0);
  }

  get times() {
    return this.allTimes.filter(time => !time.deleted);
  }

  get allTimes() {
    const { timeList, comparator } = TimeListTable.get(this);
    const array = timeList instanceof TimeList ? timeList.times : Array.prototype.slice.call(timeList);
    if(comparator) array.sort(comparator);
    return array;
  }
}

class TimeCache extends EventEmitter {
  constructor(service) {
    super();
    TimeListTable.set(this, { service, entries: {}, entriesByDay: {} });
  }

  async list(from, to, partition = 24*3600*1000) {
    const { service, entriesByDay } = TimeListTable.get(this);

    from = moment(from).startOf('day').toDate();
    to = moment(to).startOf('day').toDate();

    // make sure we have all partitions in the cache
    let partitions = [];
    for(let i = from.getTime(); i < to.getTime(); i += partition) {
      const startDay = new Date(i).toISOString();
      if(!(startDay in entriesByDay)) {
        partitions.push(startDay);
      }
    }
    if(partitions.length) {
      await service.fetch(partitions);
    }

    let out = [];
    for(let i = from.getTime(); i < to.getTime(); i += partition) {
      const startDay = new Date(i).toISOString();
      // create the list if it doesn't already exist
      if(!entriesByDay[startDay]) {
        const array = [];
        const list = new TimeList(service, array, startDay);
        entriesByDay[startDay] = { array, list };
      }

      out.push(entriesByDay[startDay].list);
    }

    return out;
  }

  get running() {
    const { entries } = TimeListTable.get(this);
    return Object.values(entries).find(time => time.running) || null;
  }

  update(time, json) {
    const { entries } = TimeListTable.get(this);

    this.set(json);
  }

  set(time) {
    const { service, entries, entriesByDay } = TimeListTable.get(this);

    // switch from localId to id when update comes from server
    if(time.id && !entries[time.id] && entries[time.localId]) {
      const tmp = entries[time.localId];
      delete entries[time.localId];
      entries[time.id] = tmp;
    }

    // generate localId when id is not available yet (local create)
    let localId = time.id ? time.id : time.localId;
    if(!localId) {
      time.localId = localId = `local-${uuid()}`;
    }
    const old = entries[localId];
    let listRemoved, listAdded;

    if(time.running || old && old.running && !time.running) {
      service.emit('running');
    }

    if(entries[localId]) {
      TimeTable.set(entries[localId], time);
      entries[localId].emit('update', entries[localId]);
    } else {
      entries[localId] = new Time(service, time);
      listAdded = true;
    }
    time = entries[localId];

    const startDay = moment(time.created).startOf('day').toDate().toISOString();
    if(old) {
      // remove the previous entry from entriesByDay
      const oldStartDay = moment(old.created).startOf('day').toDate().toISOString();
      if(entriesByDay[oldStartDay] && oldStartDay !== startDay) {
        const oldIndex = entriesByDay[oldStartDay].array.findIndex(t => t.id === time.id);
        entriesByDay[oldStartDay].array.splice(oldIndex, 1);
        listRemoved = entriesByDay[oldStartDay].list;
      }
    }

    // add the new entry to entriesByDay
    if(!entriesByDay[startDay]) {
      const array = [];
      const list = new TimeList(service, array, startDay);
      entriesByDay[startDay] = { array, list };
    }
    if(listAdded) {
      entriesByDay[startDay].array.push(time);
    }

    // emit update events for the lists
    if(listRemoved && listRemoved !== entriesByDay[startDay].list) {
      listRemoved.emit('update', listRemoved);
    }
    if(listAdded) {
      entriesByDay[startDay].list.emit('update', entriesByDay[startDay].list);
    }

    return time;
  }

  delete(time) {

  }

  clear() {
    Object.assign(TimeListTable.get(this), { entries: {}, entriesByDay: {} });
  }
}

const startTimeRunning = (time, date = new Date()) => {
  time = time.toJSON();
  if(!time.running) {
    time.started = date.toISOString();
    time.ended = null;
    time.running = true;
  }
  return time;
}

const stopTimeRunning = (time, date = new Date()) => {
  time = time.toJSON();
  if(time.running) {
    time.ended = date.toISOString();
    time.duration += (new Date(time.ended).getTime() - new Date(time.started).getTime()) / (3600*1000);
    time.running = false;
  }
  return time;
}


export default class TimeService extends EventEmitter {
  constructor(serviceManager) {
    super();

    serviceManager.on('ready', () => {
      const { userService, websocketService } = serviceManager;

      let _id = null;
      userService.on('update', (user) => {
        const { id } = user || { id: null };
        if(_id === id) {
          return;
        }
        _id = id;

        const { cache } = TimeServiceTable.get(this);
        cache.clear();
      });

      userService.once('ready', async () => {
        try {
          if(userService.user) {
            await this.list().catch(() => {});
          }
        } finally {
          TimeServiceTable.get(this).ready = true;
          this.emit('ready');
        }
      });

      websocketService.on('update.time', (time) => {
        const { cache } = TimeServiceTable.get(this);
        cache.set(time);
      });
    });

    TimeServiceTable.set(this, {
      serviceManager,
      get userService() {
        return serviceManager.userService;
      },
      cache: new TimeCache(this)
    });
  }

  async create(description, duration = 0) {
    const { userService, cache } = TimeServiceTable.get(this);

    // synchronously create the time locally so that network instabilities do
    // not affect the behaviour of the user interface, this will be updated on
    // the other side by the websocket and the generated localId will be replaced
    // with an id
    const timestamp = this.getCurrentDate();
    let running = cache.running;
    if(running) {
      running = stopTimeRunning(running, timestamp);
      cache.set(running);
    }
    const time = cache.set({
      created: timestamp.toISOString(),
      started: timestamp.toISOString(),
      ended: null,
      duration,
      running: true,
      description
    });

    const accessToken = await userService.getAccessToken();

    let response = await fetch(`${Config.URL_PREFIX}/time/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${accessToken}`
      },
      body: JSON.stringify({
        localId: time.localId,
        created: timestamp.toISOString(),
        started: timestamp.toISOString(),
        duration,
        description
      })
    });
    response = await response.json();
    return response;
  }

  clearCache() {
    //Object.assign(TimeServiceTable.get(this), { cache: null });
  }

  async fetch(partitions) {
    const { userService, cache } = TimeServiceTable.get(this);
    const accessToken = await userService.getAccessToken();

    const qs = partitions.map(x => `date=${encodeURIComponent(new Date(x).toISOString())}`).join('&');

    let response = await fetch(`${Config.URL_PREFIX}/time/?${qs}`, {
      method: 'GET',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      },
    });
    response = await response.json();

    const times = response.reduce((a, b) => [...a, ...b.times], []);
    times.forEach(time => cache.set(time));
  }

  async list(date) {
    const { cache } = TimeServiceTable.get(this);

    date = date ? new Date(date) : new Date(this.getCurrentDate());
    const startWeek = moment(date).startOf(this.getMomentWeekStyle()).toDate();
    const endWeek = moment(startWeek).add(7, 'days').toDate();
    const startCache = moment(startWeek).add(-28, 'days').toDate();
    const endCache = moment(endWeek).add(28, 'days').toDate();
    const list = await cache.list(startWeek, endWeek);
    cache.list(startCache, endCache);
    return list;
  }

  async startTime(time) {
    const { userService, cache } = TimeServiceTable.get(this);

    if(time.running) {
      throw new Error("Time is already running");
    }

    // perform task locally synchronously to avoid UI delays, this will be
    // updated with server response via websockets when this task completes
    const timestamp = this.getCurrentDate();
    let running = this.getRunning();
    if(running) {
      running = stopTimeRunning(running, timestamp);
      cache.set(running);
    }
    time = startTimeRunning(time, timestamp);
    cache.set(time);

    const accessToken = await userService.getAccessToken();
    await fetch(`${Config.URL_PREFIX}/time/${time.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    });
  }

  async stopTime(time) {
    const { userService, cache } = TimeServiceTable.get(this);

    if(!time.running) {
      throw new Error("Time is not running");
    }

    // perform task locally synchronously to avoid UI delays, this will be
    // updated with server response via websockets when this task completes
    const timestamp = this.getCurrentDate();
    time = stopTimeRunning(time, timestamp);
    cache.set(time);

    const accessToken = await userService.getAccessToken();
    await fetch(`${Config.URL_PREFIX}/time/${time.id}`, {
      method: 'PUT',
      headers: {
        'Authorization': `Bearer ${accessToken}`
      }
    });
  }

  getRunning() {
    const { userService } = TimeServiceTable.get(this);
    const { cache } = TimeServiceTable.get(this);
    return cache.running;
  }

  getCurrentDate() {
    return moment().toDate();
  }

  getMomentWeekStyle() {
    const { userService } = TimeServiceTable.get(this);
    if(userService.user) {
      return userService.user.weekStarts === 'monday' ? 'isoweek' : 'week';
    } else {
      return 'isoweek';
    }
  }

  get ready() {
    const { ready } = TimeServiceTable.get(this);
    return ready;
  }
}
