
import { EventEmitter } from 'events';

import WindowService from './window-service/index';
import UserService from './user-service';
import TimeService from './time-service';
import WebSocketService from './websocket-service';
import NetworkService from './network-service/index';

export default class ServiceManager extends EventEmitter {
  constructor(options) {
    super();
    this.userService = new UserService(this, options);
    this.timeService = new TimeService(this, options);
    this.windowService = new WindowService(this, options);
    this.websocketService = new WebSocketService(this, options);
    this.networkService = new NetworkService(this, options);
    this.emit('ready');
  }

  shutdown() {
    for(var v in this) {
      if(typeof this[v] === 'object' && this[v] && this[v].shutdown) {
        this[v].shutdown();
      }
    }
  }

}
