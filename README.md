
## Setting up Android SDK on MacOS

```
brew cask install android-sdk
```

Add this to your ~/.bash_profile:
```
export ANDROID_SDK_ROOT=/usr/local/share/android-sdk
```


Find the latest SDK to install:
```
sdkmanager --list --verbose
```

Install the Android v26 SDK:
```
sdkmanager \
  'system-images;android-26;google_apis;x86' \
  'platforms;android-26' \
  'platform-tools' \
  'extras;intel;Hardware_Accelerated_Execution_Manager' \
  'emulator' \
  'build-tools;26.0.1'
cd /usr/local/share/android-sdk/extra/intel/Hardware_Accelerated_Execution_Manager/
sudo ./silent_install.sh
```

Create the AVD:
```
avdmanager \
  --verbose \
  create avd \
  --force \
  --name x86 \
  --device "Nexus 6" \
  --package "system-images;android-26;google_apis;x86" \
  --tag "google_apis" \
  --abi "x86"
```

Run the emulator:
```
yarn start:android-emulator
```
