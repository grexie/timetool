
import mailgun from 'mailgun-js';
import mailcomposer from 'mailcomposer';

const mailer = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN
});

export default async (user, subject, text, html, context = {}) => new Promise((resolve, reject) => {

  const { app } = require('./index');

  app.render(text, { user, URL_PREFIX: process.env.URL_PREFIX, layout: 'text', ...context }, (err, text) => {
    if(err) {
      reject(err);
      return;
    }

    app.render(html, { user, URL_PREFIX: process.env.URL_PREFIX, layout: 'email', ...context }, (err, html) => {
      const mail = mailcomposer({
        from: '"Timetool by Grexie" <noreply@grexie.com>',
        to: user.email,
        subject,
        text, html
      });

      mail.build((err, message) => {
        if(err) {
          reject(err);
          return;
        }

        mailer.messages().sendMime({
          to: user.email,
          message: message.toString('ascii')
        }, (err, body) => {
          if(err) {
            reject(err);
            return;
          }

          resolve(body);
        });
      });
    });
  });
});
