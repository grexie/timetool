
import WebSocket from 'ws';
import url from 'url';

export default (wss, req) => {
  const location = url.parse(req.url, true);
  const wsc = new WebSocket(`ws://client:8080/${location.path}`);

  wsc.on('message', (data) => wss.send(data));
  wss.on('message', (data) => wsc.send(data));
  wsc.on('close', () => wss.close());
  wss.on('close', () => wsc.close());
  wsc.on('error', () => {});
  wss.on('error', () => {});
};
