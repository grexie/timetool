


import mongoose from 'mongoose';
import url from 'url';
import { redis } from '../redis';

export default (ws, req) => {
  const User = mongoose.model('User');
  let started = new Date();
  let user;

  const location = url.parse(req.url, true);
  const { u: accessToken } = location.query;

  let subscriber;

  ws.on('message', (message) => {
    const { type, body } = JSON.parse(message);
    if(type === 'keepalive') {
      return;
    }

    ws.close();
  });

  ws.on('close', (message) => {
    console.info(`[wss] ${req.ip || req.connection.remoteAddress} - ${user.email} - onclose - ${Math.floor((new Date().getTime() - started.getTime()) / 1000)} s`);
    subscriber.unsubscribe();
    subscriber.quit();
  });

  let q = User.findOne({ accessToken }).exec();
  q = q.then((_user) => {
    user = _user;
    const { id } = user.toJSON();
    console.info(`[wss] ${req.ip || req.connection.remoteAddress} - ${user.email} - onopen`);

    subscriber = redis();
    subscriber.on('message', (channel, message) => {
      message = JSON.parse(message);
      ws.send(JSON.stringify(message));
    });
    subscriber.subscribe(`user:${id}`);
  });
  q.catch((err) => console.error(err));
};
