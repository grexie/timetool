
import WebSocket from 'ws';
import mongoose from 'mongoose';
import url from 'url';

import onClientConnection from './client';
import onWebpackConnection from './wds.js';

export default (server) => {
  const wss = new WebSocket.Server({ server });

  wss.on('connection', (ws, req) => {
    const location = url.parse(req.url, true);

    if(/^\/sockjs-node\//.test(location.pathname)) {
      onWebpackConnection(ws, req);
    } else if(location.query.u) {
      onClientConnection(ws, req);
    } else {
      ws.close();
    }
  });
};
