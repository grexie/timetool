
import url from 'url';
import path from 'path';
import http from 'http';
import express from 'express';
import bodyParser from 'body-parser';
import handlebars from 'express-handlebars';
import routerTree from './router-tree';
import proxy from 'express-http-proxy';

import createWebSocketServer from './websockets';

import morgan from 'morgan';

import './mongo';

const app = exports.app = express();
const server = http.createServer(app);
createWebSocketServer(server);

app.enable('trust proxy');
app.engine('hbs', handlebars({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: path.resolve(__dirname, 'views', 'layouts'),
  partialsDir: path.resolve(__dirname, 'views', 'partials'),
  helpers: {
    json: (context, block) => JSON.stringify(context)
  }
}));
app.set('view engine', 'hbs');
app.set('views', path.resolve(__dirname, 'views'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if(process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackMiddleware = require('webpack-dev-middleware');
  const webpackConfig = require('../../webpack.config');

  app.use(webpackMiddleware(webpack(webpackConfig), {
    publicPath: '/assets/'
  }));
}

morgan.token('path', (req, res) => url.parse(req.originalUrl).pathname);
morgan.token('user', (req, res) => {
  if(req.user) {
    return req.user.email;
  } else {
    return 'anonymous';
  }
});

const routes = express.Router();
const morganSkip = new WeakMap();
routes.use(morgan('[https] :remote-addr - :user - :method :path :status - :response-time ms', {
  skip: (req, res) => morganSkip.has(req)
}));
routes.use(routerTree(path.resolve(__dirname, 'routes')));
routes.use((req, res, next) => {
  morganSkip.set(req, true);
  next();
})
app.use(routes);

app.use('/assets/', express.static(path.resolve(__dirname, '..', 'static')));

app.use(proxy('client:8080', {
  preserveHostHdr: true
}));


server.listen(process.env.PORT || 3000, (err) => {
  console.info(`Listening on port ${server.address().port}`);
});
