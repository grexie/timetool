
import express from 'express';
export const router = express.Router();

router.get('/:echo', (req, res) => res.json({ status: 'success', echo: req.params.echo, timestamp: new Date().toISOString() }));
