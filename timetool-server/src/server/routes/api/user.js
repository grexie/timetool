
import express from 'express';
import mongoose from 'mongoose';
import scrypt from 'scrypt';
import crypto from 'crypto';
import { base16, base62, uuid } from '../../utils';
import sendmail from '../../mailgun';
import { redis } from '../../redis';

const router = express.Router();
export { router };

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: {
      hash: {
        type: String,
        required: true
      },
      salt: {
        type: String,
        required: true
      }
    },
    required: true
  },
  validated: {
    type: Boolean,
    required: false,
    default: false
  },
  validationToken: {
    type: String,
    required: false,
    default: () => uuid(2)
  },
  lostPasswordToken: {
    type: String,
    required: false,
    default: null
  },
  accessToken: {
    type: String,
    unique: true,
    required: true
  },
  pushToken: {
    type: String,
    default: null
  },
  created: {
    type: Date,
    required: true,
    default: Date
  },
  weekStarts: {
    type: String,
    enum: ['monday', 'sunday'],
    default: 'monday',
    required: true
  }
}, {
  id: false,
  toJSON: {
    virtuals: true,
    transform: (doc, ret) => {
      delete ret._id;
      delete ret.__v;

      delete ret.validationToken;
      delete ret.accessToken;
      delete ret.password;
    }
  }
});
schema.virtual('id').get(function() {
  return base62.encode(base16.decode(this._id.toString()));
});
schema.method('setPassword', function(password, next) {
  crypto.randomBytes(32, (err, salt) => {
    if(err) {
      next(err);
      return;
    }

    scrypt.hash(password, { N: 16, r: 1, p: 1 }, 64, salt, (err, hash) => {
      if(err) {
        next(err);
        return;
      }

      salt = salt.toString('base64');
      hash = hash.toString('base64');
      this.password = { hash, salt };
      next(null, this);
    });
  });
});
schema.method('verifyPassword', function(password, next) {
  const salt = Buffer.from(this.password.salt, 'base64');

  scrypt.hash(password, { N: 16, r: 1, p: 1 }, 64, salt, (err, hash) => {
    if(err) {
      next(err);
      return;
    }

    hash = hash.toString('base64');
    next(null, this.password.hash === hash);
  });
});
mongoose.model('User', schema);

// register user, returns login token
router.post('/', (req, res, next) => {
  const User = mongoose.model('User');
  const { name, email, password } = req.body;

  crypto.randomBytes(32, (err, salt) => {
    if(err) {
      next(err);
      return;
    }

    crypto.randomBytes(32, (err, accessToken) => {
      if(err) {
        next(err);
        return;
      }

      scrypt.hash(password, { N: 16, r: 1, p: 1 }, 64, salt, (err, hash) => {
        if(err) {
          next(err);
          return;
        }

        salt = salt.toString('base64');
        hash = hash.toString('base64');
        accessToken = accessToken.toString('base64');

        const user = new User({
          name,
          email,
          password: { hash, salt },
          accessToken
        });

        user.save((err, user) => {
          if(err) {
            if(/duplicate key error/.test(err.message)) {
              res.status(403).json(null);
              return;
            }

            next(err);
            return;
          }

          sendmail(user, 'Your Timetool account', 'emails/validate-text', 'emails/validate-html');

          const json = user.toJSON();
          json.accessToken = user.accessToken;
          res.json(json);
        });
      });
    });
  });
});

const testUser = new (mongoose.model('User'))({
  name: 'Example User',
  email: 'user@example.com'
});

router.get('/test/emails/validate-html', (req, res) => res.render('emails/validate-html', { user: testUser, layout: 'email' }));
router.get('/test/emails/validate-text', (req, res) => { res.set('Content-Type', 'text/plain'); res.render('emails/validate-text', { user: testUser, layout: 'text' }); });
router.get('/test/validate-not-found', (req, res) => res.render('validate-not-found', { user: testUser, layout: 'email' }));
router.get('/test/validate-already-valid', (req, res) => res.render('validate-already-valid', { user: testUser, layout: 'email' }));
router.get('/test/validate-done', (req, res) => res.render('validate-already-valid', { user: testUser, layout: 'email' }));

router.get('/validate/:email/:validationToken', (req, res, next) => {
  const User = mongoose.model('User');
  const { email, validationToken } = req.params;

  User.findOne({ email, validationToken }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.render('validate-not-found', { user: { email }, layout: 'email' });
      return;
    }

    if(user.validated) {
      res.render('validate-already-valid', { user, layout: 'email' });
      return;
    }

    user.validated = true;
    user.save((err, user) => {
      if(err) {
        next(err);
        return;
      }

      const db = redis();
      db.publish(`user:${user.id}`, JSON.stringify({ type: 'update.user', body: user }));
      db.quit();

      res.render('validate-done', { user, layout: 'email' });
    });
  });
});

export const auth = (req, res, next) => {
  const User = mongoose.model('User');

  let accessToken = req.get('Authorization') && req.get('Authorization').match(/^Bearer (.+)$/);
  if(!accessToken || !accessToken[1]) {
    res.status(403).json(null);
    return;
  }
  accessToken = accessToken[1];

  User.findOne({ accessToken }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.status(403).json(null);
      return;
    }

    req.user = user;
    next();
  });
};

// retrieve access token
router.put('/', (req, res, next) => {
  const User = mongoose.model('User');
  const { email, password } = req.body;

  User.findOne({ email }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.status(403).json(null);
      return;
    }

    let { hash, salt } = user.password;
    salt = Buffer.from(salt, 'base64');

    scrypt.hash(password, { N: 16, r: 1, p: 1 }, 64, salt, (err, hashCheck) => {
      if(err) {
        next(err);
        return;
      }

      hashCheck = hashCheck.toString('base64');

      if(hash !== hashCheck) {
        res.status(403).json(null);
        return;
      }

      const json = user.toJSON();
      json.accessToken = user.accessToken;
      res.json(json);
    });
  });
});

// get authenticated user
router.get('/me', auth, (req, res, next) => {
  res.json(req.user);
});

router.put('/me', auth, (req, res, next) => {
  const { weekStarts, pushToken } = req.body;

  const user = req.user;
  user.weekStarts = weekStarts;
  user.pushToken = pushToken;

  user.save((err, user) => {
    if(err) {
      next(err);
      return;
    }

    res.json(user);
  });
});

router.post('/me/change-password', auth, (req, res, next) => {
  const { user } = req;
  const { current, password } = req.body;

  user.verifyPassword(current, (err, valid) => {
    if(err) {
      next(err);
      return;
    }

    if(!valid) {
      res.status(403).json({ error: 'Current password is not correct'});
      return;
    }

    user.setPassword(password, (err, user) => {
      if(err) {
        next(err);
        return;
      }

      user.save((err, user) => {
        if(err) {
          next(err);
          return;
        }

        res.status(200).json(null);
      });
    });
  });
});

router.post('/me/validate', auth, (req, res, next) => {
  const { user } = req;
  if(user.validated) {
    res.status(403).json(null);
    return;
  }

  sendmail(user, 'Your Timetool account', 'emails/validate-text', 'emails/validate-html');
  res.json(null);
});

router.post('/lost-password', (req, res, next) => {
  const User = mongoose.model('User');
  const { email } = req.body;

  User.findOne({ email }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.status(403).json(null);
      return;
    }

    if(!user.lostPasswordToken) {
      user.lostPasswordToken = uuid(2);
      user.save((err, user) => {
        if(err) {
          next(err);
          return;
        }

        sendmail(user, 'Reset password', 'emails/lost-password-text', 'emails/lost-password-html');
        res.json(null);
      });
    } else {
      sendmail(user, 'Reset password', 'emails/lost-password-text', 'emails/lost-password-html');
      res.json(null);
    }
  });
});

router.get('/lost-password/not-found', (req, res) => res.render('lost-password-not-found', { layout: 'email' }));
router.get('/lost-password/changed', (req, res) => res.render('lost-password-changed', { layout: 'email' }));

router.get('/lost-password/:email/:lostPasswordToken', (req, res, next) => {
  const User = mongoose.model('User');
  const { email, lostPasswordToken } = req.params;

  User.findOne({ email, lostPasswordToken }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.redirect('/api/user/lost-password/not-found');
      return;
    }

    res.render('lost-password-form', { user, layout: 'email' });
  });
});

router.post('/lost-password/:email/:lostPasswordToken', (req, res, next) => {
  const User = mongoose.model('User');
  const { email, lostPasswordToken } = req.params;
  const { password, confirm } = req.body;

  User.findOne({ email, lostPasswordToken }).exec((err, user) => {
    if(err) {
      next(err);
      return;
    }

    if(!user) {
      res.redirect('/api/user/lost-password/not-found');
      return;
    }

    if(password !== confirm) {
      res.render('lost-password-form', { user, layout: 'email', error: 'Passwords do not match', password, confirm });
      return;
    }

    user.setPassword(password, (err, user) => {
      if(err) {
        next(err);
        return;
      }

      user.lostPasswordToken = null;
      user.save((err, user) => {
        if(err) {
          next(err);
          return;
        }

        res.redirect('/api/user/lost-password/changed');
      });
    });
  });
});
