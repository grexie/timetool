
import express from 'express';
import mongoose, { Schema } from 'mongoose';
import moment from 'moment';
import { auth } from './user';
import { redis } from '../../redis';
import { uuid, base16, base62 } from '../../utils';

const router = express.Router();
export { router };

const schema = new mongoose.Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  localId: {
    type: String,
    required: false,
    default: () => `local-${uuid()}`
  },
  description: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    required: true,
    index: true,
    default: Date
  },
  deleted: {
    type: Date,
    default: null
  },
  started: {
    type: Date,
    required: true,
    index: true,
    default: Date
  },
  ended: {
    type: Date,
    index: true,
    default: null
  },
  duration: {
    type: Number,
    default: 0
  }
}, {
  id: false,
  toJSON: {
    virtuals: true,
    transform: (doc, ret) => {
      delete ret._id;
      delete ret.__v;

      ret.user = base62.encode(base16.decode(ret.user.toString()));
    }
  }
});
schema.virtual('id').get(function() {
  return base62.encode(base16.decode(this._id.toString()));
});
schema.virtual('running').get(function() {
  return this.ended === null;
});
schema.method('end', function() {
  endTime(this);
});
mongoose.model('Time', schema);


const endTime = (time) => {
  time.ended = new Date();
  time.duration += (time.ended.getTime() - time.started.getTime()) / (3600*1000);
};

// create times
router.post('/', auth, (req, res, next) => {
  const Time = mongoose.model('Time');

  const { created, started, description, duration, localId } = req.body;

  const time = new Time({ user: req.user._id, created, started, description, duration, localId });

  time.save((err, time) => {
    if(err) {
      next(err);
      return;
    }

    const db = redis();
    db.publish(`user:${req.user.id}`, JSON.stringify({ type: 'update.time', body: time }));
    db.quit();

    Time.find({
      $and: [
        { user: req.user._id },
        { _id: { $ne: time._id } },
        { ended: null },
      ]
    }).exec((err, times) => {
      if(err) {
        next(err);
        return;
      }

      let q = Promise.all(times.map(time => new Promise((resolve, reject) => {
        endTime(time);

        time.save((err, time) => {
          if(err) {
            reject(err);
            return;
          }

          const db = redis();
          db.publish(`user:${req.user.id}`, JSON.stringify({ type: 'update.time', body: time }));
          db.quit();

          resolve(time);
        })
      })));
      q = q.then(() => res.json(time));
      q.catch(next);
    });
  });
});

// start / stop time
router.put('/:id', auth, (req, res, next) => {
  const Time = mongoose.model('Time');

  const _id = base16.encode(base62.decode(req.params.id));

  Time.findOne({ _id, user: req.user._id }).exec((err, time) => {
    if(err) {
      next(err);
      return;
    }

    if(!time.ended) {
      // end the timer
      endTime(time);
    } else {
      // start the timer
      time.started = new Date();
      time.ended = null;
    }

    time.save((err, time) => {
      if(err) {
        next(err);
        return;
      }

      const db = redis();
      db.publish(`user:${req.user.id}`, JSON.stringify({ type: 'update.time', body: time }));
      db.quit();

      if(time.ended === null) {
        Time.find({
          $and: [
            { user: req.user._id },
            { _id: { $ne: _id } },
            { ended: null },
          ]
        }).exec((err, times) => {
          if(err) {
            next(err);
            return;
          }

          let q = Promise.all(times.map(time => new Promise((resolve, reject) => {
            endTime(time);

            time.save((err, time) => {
              if(err) {
                reject(err);
                return;
              }

              const db = redis();
              db.publish(`user:${req.user.id}`, JSON.stringify({ type: 'update.time', body: time }));
              db.quit();

              resolve(time);
            })
          })));
          q = q.then(() => res.json(time));
          q.catch(next);
        });
      } else {
        res.json(time);
      }
    });
  });
});

const getMomentWeekStyle = (user) => {
  if(!user) return 'isoweek';

  if(user.weekStarts === 'sunday') return 'week';
  else return 'isoweek';
};

// list times
router.get('/', auth, (req, res, next) => {
  const Time = mongoose.model('Time');

  let { date } = req.query;
  if(!Array.isArray(date)) date = [date];
  date = date.map(date => new Date(date));

  let q = Promise.all(date.map((startDate) => {
    const endDate = moment(startDate).add(1, 'days').toDate();

    const times = Time.find({
      $and: [
        { user: req.user._id },
        { created: { $gte: startDate } },
        { created: { $lt: endDate } },
        { deleted: null }
      ]
    }).sort('-created').exec();

    const totals = Time.aggregate([
      { $match: { $and: [ { user: req.user._id }, { created: { $gte: startDate } }, { created: { $lt: endDate } } ] } },
      { $project: { _id: '$user', duration: 1 } },
      { $group: { _id: 0, total: { $sum: '$duration' } } }
    ]).cursor({ batchSize: 2500, async: true }).exec().then(cursor => cursor.toArray());

    return Promise.all([times, totals]).then(([times, totals]) => {
      return {
        date: startDate,
        total: (totals[0] || {}).total || 0,
        times
      };
    });
  }));

  q = q.then(rows => res.json(rows));
  q.catch(next);
});
