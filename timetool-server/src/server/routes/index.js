
import express from 'express';
import mongoose from 'mongoose';
export const router = express.Router();

//router.get('/', (req, res) => res.render('home', { layout: 'site', lightContent: true }));


router.get('/apple-app-site-association', (req, res) => res.json({
  applinks: {
    apps: [],
    details: [
      {
        "appID": "RY2J5VXCDH.com.grexie.Timetool",
        "paths": ["*"]
      }
    ]
  }
}));
