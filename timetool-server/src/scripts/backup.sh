#!/bin/bash

docker stop timetool_mongo_1
docker run --rm \
  -v timetool_mongo:/data:ro \
  -v $(pwd):/backup \
  ubuntu \
  tar cvzf /backup/backup-$(date +%Y-%m-%dT%H-%m-%S).tar.gz /data
docker start timetool_mongo_1
