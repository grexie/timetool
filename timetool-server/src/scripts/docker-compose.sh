#!/bin/bash -e

if [ "${NODE_ENV}" == "production" ]; then
  echo NODE_ENV=production
  docker-compose -f docker-compose.prod.yml -p timetool $@
else
  echo NODE_ENV=development
  docker-compose -f docker-compose.dev.yml -p timetool-dev $@
fi
