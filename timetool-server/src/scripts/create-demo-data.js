
import '../server/mongo';
import '../server/routes/api/user';
import '../server/routes/api/time';

import mongoose from 'mongoose';
import moment from 'moment';
import { redis } from '../server/redis';


const events = [
  { description: 'Call Jane', duration: 1, delta: 0.5 },
  { description: 'Call Paul', duration: 1, delta: 0.5 },
  { description: 'Call Debbie', duration: 1, delta: 0.5 },
  { description: 'Call Jon', duration: 1, delta: 0.5 },
  { description: 'Call James', duration: 1, delta: 0.5 },
  { description: 'Buy food for dinner', duration: 1, delta: 0.5 },
  { description: 'Prepare slides for marketing meeting', duration: 5, delta: 1 },
  { description: 'Have lunch with team', duration: 1, delta: 0.5 },
  { description: 'Meet Paul', duration: 2, delta: 1 },
  { description: 'Interviews', duration: 5, delta: 3 },
];


const deleteEntries = async (user) => {
  const Time = mongoose.model('Time');
  const times = await Time.find({ user });
  const db = redis();
  const timestamp = new Date();
  for(const time of times) {
    time.deleted = timestamp;
    if(time.running) {
      time.end();
    }
    await time.remove();
    db.publish(`user:${user.id}`, JSON.stringify({ type: 'update.time', body: time }));
  }
  return new Promise((resolve) => db.quit(() => resolve()));
};

const delta = (duration, delta) => Math.max(0, duration - delta / 2 + delta * Math.random());

const createEntries = async (user, days = 365) => {
  const Time = mongoose.model('Time');

  const startToday = moment().startOf('day').toDate();
  const db = redis();

  for(let i = -days; i <= 0; i++) {
    let startTime = moment(startToday).add(i, 'days').add(delta(8, 1), 'hours').toDate();
    const eventsPool = events.slice();
    const maxDuration = i === 0 ? new Date().getHours() - 8 : 8;
    let totalDuration = 0;
    while(totalDuration < maxDuration) {
      const [event] = eventsPool.splice(Math.floor((eventsPool.length - 1) * Math.random()), 1);
      const duration = delta(event.duration, event.delta);
      let time = new Time({
        user: user._id,
        description: event.description,
        created: startTime,
        started: startTime,
        ended: moment(startTime).add(duration, 'hours').toDate(),
        duration
      });
      totalDuration += duration;
      startTime = moment(time.ended).add(delta(0.5, 0.5), 'hours').toDate();
      time = await time.save();
      db.publish(`user:${user.id}`, JSON.stringify({ type: 'update.time', body: time }));
    }
  }

  await new Promise((resolve) => db.quit(() => resolve()));
};

const getUser = async (email) => {
  const User = mongoose.model('User');
  const user = await User.findOne({ email }).exec();
  if(!user) throw new Error(`Could not find user for email ${email}`);
  return user;
};

const main = async () => {
  const user = await getUser(process.argv[2]);
  await deleteEntries(user);
  await createEntries(user);
};

main().then(() => process.exit(0), (err) => {
  console.error(err);
  process.exit(1);
});
