#!/bin/bash

docker stop timetool_mongo_1
docker run -i --rm \
  --volume timetool_mongo:/data \
  --volume $(pwd)/$1:/backup.tar.gz \
  ubuntu bash -s <<EOF
rm -Rf /data/*
tar xvzf /backup.tar.gz -C /data --strip 1
EOF
docker start timetool_mongo_1
